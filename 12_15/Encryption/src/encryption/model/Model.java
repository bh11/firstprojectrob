/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryption.model;

import encryption.controller.MyController;

/**
 *
 * @author Leo
 */
public interface Model {
    
     void setController(MyController c);
     
     void setText(String s);
     
     String getText();
    
}
