/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryption.model;

import encryption.controller.MyController;

/**
 *
 * @author Leo
 */
public class EncryptedText implements Model {
    private MyController controller;
    
    private String result = new String();

    @Override
    public void setController(MyController c) {
        controller = c;
    }

    @Override
    public void setText(String s) {
        result = s;
        controller.notifyView();
    }

    @Override
    public String getText() {
        return result;
    }
    
    
}
