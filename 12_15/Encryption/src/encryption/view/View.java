/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryption.view;

import encryption.controller.MyController;

/**
 *
 * @author Leo
 */
public interface View {
    
    void setController(MyController c);
    void setText(String s);
    void start();
    
}
