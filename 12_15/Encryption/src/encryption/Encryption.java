/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encryption;

import encryption.controller.Controller;
import encryption.controller.MyController;
import encryption.model.EncryptedText;
import encryption.model.Model;
import encryption.view.SwingView;
import encryption.view.View;

/**
 *
 * @author Leo
 */
public class Encryption {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        View v = new SwingView();
        Model m = new EncryptedText();
        Controller c = new MyController(v, m);
        
        v.start();
    }
    
}
