/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import controller.Controller;
import model.DataModel;
import view.View;

/**
 *
 * @author Leo
 */
public class Calculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        View v = new View();
        DataModel m = new DataModel();
        Controller c = new Controller(v, m);
    }
    
}
