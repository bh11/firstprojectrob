/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Leo
 */
public enum Operator {
    ADD,
    MULTIPLY,
    EQ;
    
    public static Operator convert(String data) {
        if ("+".equals(data)) {
            return ADD;
            
        } else if ("*".equals(data)) {
            return MULTIPLY;
            
        } else if ("=".equals(data)) {
            return EQ;
            
        }
        
        return null;
    }
}
