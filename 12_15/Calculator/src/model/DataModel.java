/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Leo
 */
public class DataModel implements Serializable {

   private Controller controller;
    
    private String expression = new String();

    private int firstNumber;
    private int secondNumber;
    private Operator operator;

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public int getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }
    
    public void setController(Controller c) {
        controller = c;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.expression);
        hash = 83 * hash + this.firstNumber;
        hash = 83 * hash + this.secondNumber;
        hash = 83 * hash + Objects.hashCode(this.operator);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataModel other = (DataModel) obj;
        if (this.firstNumber != other.firstNumber) {
            return false;
        }
        if (this.secondNumber != other.secondNumber) {
            return false;
        }
        if (!Objects.equals(this.expression, other.expression)) {
            return false;
        }
        if (this.operator != other.operator) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DataModel{" + "expression=" + expression + ", firstNumber=" + firstNumber + ", secondNumber=" + secondNumber + ", operator=" + operator + '}';
    }
}
