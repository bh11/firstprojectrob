/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.Operator;

/**
 *
 * @author Leo
 */
public class View extends JFrame {
    private Controller controller;
    
    private JTextField jt = new JTextField(50);

    public View() {
        buildView();
    }
    
    public void setController(Controller c) {
        controller = c;
    }
    
    public void setText(String text) {
        jt.setText(text);
    }
    
    private JPanel generateButtons() {
        JPanel jp = new JPanel();
        jp.setLayout(new GridLayout(4, 3));
        
        int value = 1;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                JButton jb = new JButton(String.valueOf(value++));
                
                jb.addActionListener(al -> {
                    JButton jButton = (JButton) al.getSource();
                    controller.setNumber(Integer.parseInt(jButton.getText()));
                });
                
                jp.add(jb);
            }
        }
        
        JButton plus = new JButton("+");
        JButton multiply = new JButton("*");
        JButton equal = new JButton("=");
        
        plus.addActionListener(l -> controller.operate(Operator.ADD));
        multiply.addActionListener(l -> controller.operate(Operator.MULTIPLY));
        equal.addActionListener(l -> controller.operate(Operator.EQ));
        
        jp.add(plus);
        jp.add(multiply);
        jp.add(equal);
        
        return jp;
    }

    private void buildView() {
        jt.setEnabled(false);
        JPanel north = new JPanel();
        north.add(jt);
       
        add(north, BorderLayout.NORTH);
        add(generateButtons(), BorderLayout.CENTER);
        
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
