/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.DataModel;
import model.Operator;
import model.State;
import view.View;

/**
 *
 * @author Leo
 */
public class Controller {
    
    private State state = State.FIRST_NUMBER;
    
    private View v;
    private DataModel m;
    
    public Controller (View v, DataModel m) {
        this.v = v;
        this.m = m;
        
        v.setController(this);
        m.setController(this);
    }
    
    public void setNumber(int number) {
        System.out.println(number);
    }
    
    public void operate(Operator operator) {
        System.out.println(operator);
    }
    
    public String notifyView() {
        return m.getExpression();
    }
}
 