/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package credit;

import bank.User;
import bankaccount.BankAccount;

/**
 *
 * @author Leo
 */
public class FlatCredit extends BankAccount{
    private long creditAmount;
    private final double RATE_OF_INTEREST=0.06;
    private final int CREDIT_DURATION=3;
    
    private final double INTEREST = (creditAmount)/3 + (creditAmount*RATE_OF_INTEREST)/3;

    @Override
    public void moneyDeposit(long amountOfMoney) {
        this.creditAmount+=amountOfMoney;
    }

    @Override
    public void moneyWithdraw(long amountOfMoney) {
       this.creditAmount-=amountOfMoney;
    }

    @Override
    public void openingAnAccount(String accountNumber, long openingBalance, User user) {
        
    }

    @Override
    public void monthlyClosing(BankAccount acc) {
        creditAmount-=INTEREST;
    }
}
