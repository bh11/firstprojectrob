/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package credit;

/**
 *
 * @author Leo
 */
public class CreditCardCredit {
    private long balance;

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
    
    public void requestingCredit (long credit) {
        setBalance(credit);
    }
    
}
