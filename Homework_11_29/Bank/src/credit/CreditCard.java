/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package credit;

/**
 *
 * @author Leo
 */
public class CreditCard {
    private String creditCardNumber;
    private final double interest=0.06;

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }
    
    public void requestingCreditCard (CreditCardCredit credit, long creditAmount) {
        credit.requestingCredit(creditAmount);
    }
   
    
}
