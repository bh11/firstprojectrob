/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import bankaccount.BankAccount;
import bankaccount.CommunityAccount;
import bankaccount.StudentAccount;

/**
 *
 * @author Leo
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        User young=new User(14, "Pistike");
        User old=new User(40, "Béla Bá");
        
        BankAccount b1=new StudentAccount();
        b1.openingAnAccount("123456", 100, old);
        b1.openingAnAccount("123456", 100, young);
        System.out.println(young.getName()+ ","+b1.getBalance());
        
        BankAccount b2=new CommunityAccount();
        b2.openingAnAccount("457895", 50000, old);
        System.out.println(old.getName()+ ","+b2.getBalance());
    }
    
}
