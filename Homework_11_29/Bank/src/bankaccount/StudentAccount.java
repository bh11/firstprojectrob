/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccount;

import bank.User;

/**
 *
 * @author Leo
 */
public class StudentAccount extends BankAccount {

    private final int AGE_LIMIT = 18;
  

    @Override
    public void openingAnAccount(String accountNumber, long openingBalance, User user) {

        if (user.getAge() < AGE_LIMIT) {
            super.setAccountNumber(accountNumber);
            super.setBalance(openingBalance);
        } else {
            System.out.println(user.getName()+"! You are too old for a StudentAccount! ");
        }
    }

    @Override
    public void moneyDeposit(long amountOfMoney) {
       super.setBalance(super.getBalance()+amountOfMoney);
    }

    @Override
    public void moneyWithdraw(long amountOfMoney) {
        super.setBalance(super.getBalance()-amountOfMoney);
    }

    @Override
    public void monthlyClosing(BankAccount acc) {
     
    }

}
