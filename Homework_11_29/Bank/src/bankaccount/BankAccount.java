/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccount;

import bank.AbstractBank;
import bank.User;

/**
 *
 * @author Leo
 */
public abstract class BankAccount extends AbstractBank {
    private String AccountNumber;
    private long balance;
    private final int CHARGE_FOR_OPENING_AN_ACCOUNT=500;


    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String AccountNumber) {
        this.AccountNumber = AccountNumber;
    }

    public int getCHARGE_FOR_OPENING_AN_ACCOUNT() {
        return CHARGE_FOR_OPENING_AN_ACCOUNT;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
    public abstract void monthlyClosing (BankAccount acc);
    public abstract void openingAnAccount (String accountNumber, long openingBalance, User user);
    public abstract void moneyDeposit (long amountOfMoney);
    public abstract void moneyWithdraw (long amountOfMoney);
    
}
