/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccount;

import bank.User;

/**
 *
 * @author Leo
 */
public class BankCard extends BankAccount {

    private String cardNumber;
    private final int REQUESTING_CHARGE = 1000;
    private final double MONEY_WITHDRAW_RATE = 0.10;

    public BankCard(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void cardRequesting(BankAccount acc) {

        if (acc.getBalance() > REQUESTING_CHARGE) {
            acc.setBalance(acc.getBalance() - REQUESTING_CHARGE);
        } else {
            System.out.println("You do not have enough money on your account!");
        }
    }

    public void cashWithdrawWithBankCard(BankAccount acc, long amountOfMoney) {
        if ((amountOfMoney * MONEY_WITHDRAW_RATE) < REQUESTING_CHARGE) {
            acc.setBalance(acc.getBalance() - (acc.getBalance() * REQUESTING_CHARGE));
        } else {
            acc.setBalance(acc.getBalance() - REQUESTING_CHARGE);
        }
        acc.setBalance(acc.getBalance() - amountOfMoney);
    }

    public void buyWithBankCard(BankAccount acc, long payment) {
        acc.setBalance(acc.getBalance() - payment);
    }

    @Override
    public void moneyDeposit(long amountOfMoney) {
        super.setBalance(super.getBalance()+amountOfMoney);
    }

    @Override
    public void moneyWithdraw(long amountOfMoney) {
        super.setBalance(super.getBalance()-amountOfMoney);
    }

    @Override
    public void openingAnAccount(String accountNumber, long openingBalance, User user) {
        
    }

    @Override
    public void monthlyClosing(BankAccount acc) {
        
    }
}
