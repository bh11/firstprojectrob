/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccount;

import bank.User;

/**
 *
 * @author Leo
 */
public class CommunityAccount extends BankAccount {

    private final int CHARGE_FOR_ADMINISTRATION = 1500;
    private final int DISCOUNT_LIMIT = 250_000;
    private final int DISCOUNTED_CHARGE = 500;
    private final double CHARGE_RATE_FOR_MONEY_TRANSFER = 0.05;

    

    @Override
    public void openingAnAccount(String accountNumber, long openingBalance, User user) {
        if (openingBalance > super.getCHARGE_FOR_OPENING_AN_ACCOUNT()) {
            super.setAccountNumber(accountNumber);
            super.setBalance(openingBalance - super.getCHARGE_FOR_OPENING_AN_ACCOUNT());
        } else {
            System.out.println("You need more money before opening an account!");
        }
    }

    public void moneyTransfer(BankAccount senderAccount, BankAccount recieverAccount, long amountOfMoney) {
        long chargeForMoneyTransfer = (long) (amountOfMoney * CHARGE_RATE_FOR_MONEY_TRANSFER);
        senderAccount.setBalance((senderAccount.getBalance() - amountOfMoney) - chargeForMoneyTransfer);
        recieverAccount.setBalance(recieverAccount.getBalance() + amountOfMoney);
    }

    @Override
    public void moneyDeposit(long amountOfMoney) {
        super.setBalance(super.getBalance() + amountOfMoney);
    }

    @Override
    public void moneyWithdraw(long amountOfMoney) {
        super.setBalance(super.getBalance() - amountOfMoney);
    }
    
    @Override
    public void monthlyClosing(BankAccount acc) {
        if (acc.getBalance() > DISCOUNT_LIMIT) {
            acc.setBalance(acc.getBalance() - DISCOUNTED_CHARGE);
        } else {
            acc.setBalance(acc.getBalance() - CHARGE_FOR_ADMINISTRATION);
        }
    }

}
