/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import Hero.AbstractHero;
import Hero.BornOnEarth;
import Stone.StoneType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import store.Fleet;
import store.Ship;

/**
 *
 * @author Leo
 */
public class Reporting {

    private Fleet fleet;
    
    
    
    private void printCountOfStonesByShip() {
                countOfStonesOnShip().entrySet().stream()
                .forEach(i->{System.out.println("Ship: "+i);
                i.getValue()
                        .forEach((j, k)-> System.out.println("Stone: "+j+" Count: "+k));
                });
    }

    public long getCountOfBornOnEarth(List<AbstractHero> heroes) {
        return heroes.stream()
                .filter(h -> h instanceof BornOnEarth)
                .count();
    }

    public AbstractHero strongestSuperhero(List<AbstractHero> heroes) {
        return heroes.stream()
                .sorted((a, b) -> b.getPower() - a.getPower())
                .findFirst().get();
    }

    public List<Integer> identityCardNumbers(List<AbstractHero> heroes) {
        return heroes.stream()
                .filter(h -> h instanceof BornOnEarth)
                .map(i -> ((BornOnEarth) i).getIdentityCard().getNumber()).collect(Collectors.toList());
    }

    private Map<StoneType, Integer> countOfStones(Ship ship) {
        return ship.getHeroes().stream()
                .map(AbstractHero::getStone) //lista  a kövekről
                .collect(Collectors.toMap(
                                k -> k,
                                v -> 1,
                                (v1, v2) -> v1 + v2,
                                HashMap::new
                        )
                );
    }

    private Map<Ship, Map<StoneType, Integer>> countOfStonesOnShip() {
        return fleet.getShips().stream()
                .collect(Collectors.toMap(
                                s -> s,
                                this::countOfStones,
                                (x, y) -> x
                        )
                );
    }
    
    private boolean containsOnlyBornOnEarth(Ship ship) {
        return ship.getHeroes().stream()
                .allMatch(p -> p instanceof BornOnEarth);
    }
    
    private Map<Boolean, List<Ship>> partiesContainOnlyBornOnEarth (){
        return fleet.getShips().stream()
                .collect(Collectors.partitioningBy(this::containsOnlyBornOnEarth));
    }
    
//    private int numberOfShipsContainingOnlyBornOnEarth() {
//        return partiesContainOnlyBornOnEarth().get(Boolean,TRUE);
//    }

}
