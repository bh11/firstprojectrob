/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import store.Fleet;

/**
 *
 * @author Leo
 */
public class Saver {

    private final File sourceFile = new File("source.ser");

    public Fleet loader() {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(sourceFile))) {
            return (Fleet) in.readObject();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public boolean saver(Fleet f) {
        try(ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream(sourceFile))) {
            out.writeObject(f);
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
