/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import exceptions.InvalidNameAvengersException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Leo
 */
public class ConsoleReader {

    private static final String STOP = "exit";
    private LineParser parser = new LineParser();
    

    public void read() throws InvalidNameAvengersException{
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
            while(true) {
                String line = br.readLine();
                if(line == null || STOP.equals(line)) {
                    break;
                }
                parser.process(line);
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }   
        parser.printStore();
        parser.report();
        parser.save();
    }
}
