<%-- 
    Document   : welcome
    Created on : 2020. febr. 7., 19:59:21
    Author     : Leo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello <%=session.getAttribute("username")%></h1>
        <p><a href="LogoutController">logout</a></p>
    </body>
</html>
