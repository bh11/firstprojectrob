/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.cookies;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Leo
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    private final String USERNAME = "admin";
    private final String PASSWORD = "admin";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.addCookie(new Cookie("test", "elek"));
        if (USERNAME.equals(request.getParameter("user_name")) && PASSWORD.equals(request.getParameter("pwd"))) {
            request.getSession().setAttribute("logged_in", true);
            request.getSession().setAttribute("user_name", request.getParameter("user_name"));
            request.getRequestDispatcher("welcome.jsp").forward(request, response);
        } else {
            request.getSession().setAttribute("error_message", "Wrong username or pwd!!! pls. use admin/admin");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}
