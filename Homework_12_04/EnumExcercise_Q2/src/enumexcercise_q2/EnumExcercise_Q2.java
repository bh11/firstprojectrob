/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumexcercise_q2;

/**
 *
 * @author Leo
 */
public class EnumExcercise_Q2 {

    public static void dayCompare(WeekDay d) {
    WeekDay saturday = WeekDay.SATURDAY;
    int res = d.compareTo(saturday);
    if (res < 0) {
      System.out.println("The day is before " + saturday);
    } else if (res ==  0) {
      System.out.println("The day is the same with " + saturday);
    } else {
      System.out.println("The day is after " + saturday);
    }
  }
    public static void main(String[] args) {
        dayCompare(WeekDay.SUNDAY);
        dayCompare(WeekDay.FRIDAY);
        dayCompare(WeekDay.SATURDAY);
    }
    
}
