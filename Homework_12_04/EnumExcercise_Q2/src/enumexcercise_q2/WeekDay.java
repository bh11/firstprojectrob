/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumexcercise_q2;

/**
 *
 * @author Leo
 */
public enum WeekDay {

    
    MONDAY
    ,TUESDAY
    ,WEDNESDAY
    ,THURSDAY
    ,FRIDAY
    ,SATURDAY {

        @Override
        public boolean isWeekday() {
            return false;
        }
        
    }
    ,SUNDAY{
        @Override
        public boolean isWeekday() {
            return false;
        }
    };

    public boolean isWeekday() {
        return true;
    }

    public boolean isHoliday() {
        return !isWeekday();
    }
    public void daySelector (WeekDay wd) {
        if (wd.isWeekday()) {
            System.out.println("I have to work");    
        }else {
            System.out.println("It's holiday, no work!");
        }
    }
}
