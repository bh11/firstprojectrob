/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringlistsorderedwithlambdas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Leo
 */
public class StringListsOrderedWithLambdas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();

        stringList.add("alma");
        stringList.add("barack");
        stringList.add("eper");
        stringList.add("berkenye");
        stringList.add("dinnye");
        stringList.add("kivi");
        stringList.add("ribizli");
        stringList.add("mandula");
        stringList.add("narancs");

        for (String slist : stringList) {
            System.out.println(slist);
        }

        Collections.sort(stringList, (String s1, String s2) -> s1.length() - s2.length());
        System.out.println("Length shortest to longest: " + stringList);

        Collections.sort(stringList, (String s1, String s2) -> s2.length() - s1.length());
        System.out.println("Length longest to shortest: " + stringList);

        Collections.sort(stringList, (String s1, String s2) -> s1.charAt(0) - s2.charAt(0));
        System.out.println("Alphabetically by the first character: " + stringList);

        Collections.sort(stringList, (String s1, String s2) -> (s2.contains("e") ? 1 : 0) - (s1.contains("e") ? 1 : 0));
        System.out.println("Strings that contain 'e' first: " + stringList);
    }

}
