/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mavenhrquerys;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Leo
 */
public class SwingView extends JFrame {

    private JTextField tf = new JTextField("Id:", 2);
    private JTextField textFieldForId = new JTextField(7);
    private JTextArea jta = new JTextArea(35, 35);
    private JScrollPane sp = new JScrollPane(jta);

    private JButton jb1 = new JButton("Employee with maximum salary");
    private JButton jb2 = new JButton("Department with most employee");
    private JButton jb3 = new JButton("Employees with salary above average");
    private JButton jb4 = new JButton("Employees hired after 1990.01.01.");
    private JButton jb5 = new JButton("Ordered professionals containing 'clerk'");
    private JButton jb6 = new JButton("Find employee with given ID");

    DAO dao = new DAO();

    private JPanel buildNorth() {
        JPanel jp = new JPanel();
        
        jp.add(tf);
        tf.setEditable(false);
        jp.add(textFieldForId);
        
        return jp;
    }

    private JPanel buildSouth() {

        JPanel jp = new JPanel();
        jp.add(jb1);
        jp.add(jb2);
        jp.add(jb3);
        jp.add(jb4);
        jp.add(jb5);
        jp.add(jb6);
        jp.setLayout(new GridLayout(3, 2));

        jb1.addActionListener(p -> jta.setText(dao.findEmployeeWithMaxSalary()));
        jb2.addActionListener(p -> jta.setText(dao.findDepartmentWithMostEmployee()));
        jb3.addActionListener(p -> jta.setText(dao.findEmployeesWithSalaryAboveAverage()));
        jb4.addActionListener(p -> jta.setText(dao.findEmployeesHiredAfter1990_01_01()));
        jb5.addActionListener(p -> jta.setText(dao.orderedEmployeesContainsClerk()));
        jb6.addActionListener(p -> jta.setText(dao.findEmployeeById(textFieldForId.getText())));

        return jp;
    }

    private JPanel buildCenter() {

        JPanel jp = new JPanel();

        jp.add(sp);
        jta.setEditable(false);
        return jp;
    }

    public void buildWindow() {
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);
        add(buildSouth(), BorderLayout.SOUTH);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
