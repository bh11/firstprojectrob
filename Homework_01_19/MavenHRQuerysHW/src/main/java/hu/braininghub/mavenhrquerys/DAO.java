/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mavenhrquerys;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Leo
 */
public class DAO {

    private static final String URL
            = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    private static final String USER = "root";
    private static final String PW = "root";

    public String findEmployeeWithMaxSalary() {
        String sql = "SELECT * \n"
                + "\n"
                + "FROM hr.employees e \n"
                + "\n"
                + "WHERE e.salary = (SELECT max(i.salary) FROM hr.employees i);";
        String returnedString = "";

        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement()) {
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                returnedString += rs.getString("first_name") + " " + rs.getString("last_name");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return returnedString;
    }

    public String findDepartmentWithMostEmployee() {
        String sql = "SELECT B.department_name, count(*) nr\n"
                + "\n"
                + "FROM hr.employees A INNER JOIN hr.departments B\n"
                + "\n"
                + "ON A.department_id = B.department_id\n"
                + "\n"
                + "GROUP BY B.department_name\n"
                + "\n"
                + "ORDER BY nr DESC\n"
                + "\n"
                + "LIMIT 1;";
        String returnedString = "";

        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement()) {
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                returnedString += rs.getString("department_name");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return returnedString;
    }

    public String findEmployeesWithSalaryAboveAverage() {
        String sql = "SELECT first_name, last_name FROM hr.employees e WHERE e.salary > (SELECT avg(i.salary) FROM hr.employees i);";
        String returnedString = "";
        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement()) {
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                returnedString += rs.getString("first_name") + " " + rs.getString("last_name") + "\n";
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return returnedString;
    }

    public String findEmployeesHiredAfter1990_01_01() {
        String sql = "SELECT first_name, last_name, hire_date FROM hr.employees e WHERE e.hire_date > '1990.01.01';";
        String returnedString = "";
        try (
                Connection conn = DriverManager.getConnection(URL, USER, PW);
                Statement stm = conn.createStatement()) {
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                returnedString += rs.getString("first_name") + " " + rs.getString("last_name")
                        + " " + rs.getString("hire_date") + "\n";
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return returnedString;
    }

    public String orderedEmployeesContainsClerk() {

        String sql = "SELECT DISTINCT j.job_title title "
                + "FROM hr.jobs j "
                + "WHERE j.job_title "
                + "LIKE '%Clerk%' "
                + "ORDER BY title;";
        String returnedString = "";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement()) {
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                returnedString += rs.getString("title") + "\n";
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return returnedString;
    }

    public String findEmployeeById(String id) {

        String sql = "SELECT * \n"
                + "FROM employees e \n"
                + "WHERE e.employee_id = '" + id + "'";
        String returnedString = "";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement()) {

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                returnedString += rs.getString("first_name") + " " + rs.getString("last_name");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return returnedString;
    }

    public void findEmployeeByFirstName(String name) {

        String sql = "SELECT * \n"
                + "FROM employees e \n"
                + "WHERE e.first_name = ?";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement stm = connection.prepareStatement(sql)) {

            stm.setString(1, name);

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                System.out.println(rs.getString("first_name") + " " + rs.getString("last_name"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
