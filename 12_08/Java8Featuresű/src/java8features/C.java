/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8features;

/**
 *
 * @author Leo
 */
public interface C extends A{
    @Override
    default void method1() {
        System.out.println("do something in C");
    }
    static void staticMethod() {
        System.out.println("do something in static C");
    }
}
