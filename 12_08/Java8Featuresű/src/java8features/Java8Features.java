/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8features;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author Leo
 */
public class Java8Features {

    
    
    public static void main(String[] args) {
       D d=new D();
       d.method1();
       
       B.staticMethod();
       
       Stream.of("a1","a2","a3")
       .filter(p->{System.out.println("filter p: " +p);return true;}) //terminate operátor nélkül nem ír ki semmit
       .forEach(p->System.out.println("forEach: "+p));  
       
       List<Integer> intList = new ArrayList<>();
       
        for (int i = 0; i < 1000; i++) {
            intList.add(i);
        }
        // stream és parallelStream

    }
    
}
