/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package localdataand.others;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.time.format.DateTimeFormatter;

/**
 *
 * @author Leo
 */
public class LocalDateAndOthers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();

        System.out.println(localDate);

        localDate = LocalDate.of(1848, 3, 15);
        System.out.println(localDate);

//        localDate=LocalDate.parse("1985.01.19.",DateTimeFormatter.ofPattern("yyyy.MM.dd"));
//        System.out.println(localDate);
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime.format(DateTimeFormatter.ofPattern("HH")));
        localTime = LocalTime.of(6, 30);
        System.out.println(localTime);
        localTime = LocalTime.parse("05:30");
        System.out.println(localTime);

        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);

        for (String zone : ZoneId.getAvailableZoneIds()) {
            System.out.println(zone);
        }
        
    }

}
