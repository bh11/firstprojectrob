/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.factory;

/**
 *
 * @author Toth.Attila
 */
public class FactoryProvider {
    public static AbstractFactory getFactory(String type) {
        if("Auto".equals(type))
            return new AutoFactory();
        else if ("Product".equals(type))
            return new ProductFactory();
        return null;
    }
}
