/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.factory;

/**
 *
 * @author Toth.Attila
 */
//public interface AbstractFactory<T> {
public interface AbstractFactory {
    //T create(String type);
    Object create(String type);

}
