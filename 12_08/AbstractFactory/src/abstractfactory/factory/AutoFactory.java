/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.factory;

import abstractfactory.auto.Auto;
import abstractfactory.auto.BMW;
import abstractfactory.auto.Mercedes;

/**
 *
 * @author Toth.Attila
 */
//public class AutoFactory  implements AbstractFactory<Auto>{
public class AutoFactory  implements AbstractFactory{


    @Override
    public Auto create(String type) {
        if("BMW".equals(type))
            return new BMW();
        else if("Mercedes".equals(type))
            return new Mercedes();
        return null;
    }
    
}
