/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.factory;

import abstractfactory.product.Hammer;
import abstractfactory.product.Product;
import abstractfactory.product.ScrewDriver;

/**
 *
 * @author Toth.Attila
 */
//public class ProductFactory implements AbstractFactory<Product>{
public class ProductFactory implements AbstractFactory{

    @Override
    public Product create(String type) {
        if("Hammer".equals(type))
            return new Hammer();
        else if("ScrewDriver".equals(type))
            return new ScrewDriver();
        return null;
    }
    
}
