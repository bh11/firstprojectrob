/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.auto;

/**
 *
 * @author Toth.Attila
 */
public class BMW implements Auto{

    @Override
    public String getType() {
        return "BMW";
    }

    @Override
    public void drive() {
        System.out.println("driving a BMW");
    }
    
}
