/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

import abstractfactory.auto.Auto;
import abstractfactory.factory.AbstractFactory;
import abstractfactory.factory.FactoryProvider;

/**
 *
 * @author Toth.Attila
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AbstractFactory autoFactory = FactoryProvider.getFactory("Auto");
        Auto bmw = (Auto) autoFactory.create("BMW");
        bmw.drive();
    }
    
}
