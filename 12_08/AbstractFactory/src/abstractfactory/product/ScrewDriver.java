/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.product;

/**
 *
 * @author Toth.Attila
 */
public class ScrewDriver implements Product{

    @Override
    public void getWarranty() {
        System.out.println("getting screwdriver's warranty");
    }
    
}
