/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibonacci;

/**
 *
 * @author Leo
 */
public class Fibonacci {

    /**
     * @param args the command line arguments
     */
    static int fibonacci (int number) {
        if (number==0) {
            return 0;
        }
        if (number==1||number==2) {
            return 1;
        }
        int sum = 0;
        int tmp1 = 1;
        int tmp2 = 1;
        
        for (int i = 1; i < number; i++) {
            sum = tmp1 + tmp2;
            tmp1 = tmp2;
            tmp2 = sum;
        }
        return sum;
    }
    
    static int recursiveFibo(int number) {
       if (number == 0) {
           return 0;
       }        if(number == 1 || number == 2) {
           return 1;
       }        return recursiveFibo(number-1) + recursiveFibo(number - 2);
   }
    
    public static void main(String[] args) {
        
        System.out.println(fibonacci(5));
        System.out.println(recursiveFibo(5));
    }

}
