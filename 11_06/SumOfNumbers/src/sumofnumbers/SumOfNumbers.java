/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sumofnumbers;

/**
 *
 * @author Leo
 */
public class SumOfNumbers {

    /**
     * @param args the command line arguments
     */
    
    
    static int sumOfEveryNumber(int n) {
        int sum = 0;

        while (n > 0) {
            sum += n % 10;
            n /= 10;
        }
        return sum;
    }

    public static void main(String[] args) {

        System.out.println("321: " + sumOfEveryNumber(321));
    }

}
