/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaleexchange;

/**
 *
 * @author Leo
 */
public class ScaleExchange {

    /**
     * @param args the command line arguments
     */
    static String convertToBin(int number, int base) {
        /*
         9 | 1
         4 | 0
         2 | 0
         1 | 1
         0
         */ 
        String strTMP = "";
        while (number > 0) {
            strTMP = strTMP + (number % base);
            number = number / base;
        }
        String str = "";
        for (int i = strTMP.length() - 1; i >= 0; i--) {
            str = str + strTMP.charAt(i);
        }
        return str;
    }
    
    public static void decToNewBase(int num, int base) { //rekurzív
       if (num > 0) {
           decToNewBase(num / base, base);
           System.out.print(num % base);
       }
   }
    
    static void numbersToHundred (int number) {
 
        if (number!=100) {
            System.out.println(number);
            numbersToHundred(number+1);
        }
    }
    
    public static void main(String[] args) {
//       
        numbersToHundred(1);
       
        
        
        
        
    }
    
}
