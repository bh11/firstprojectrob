/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorial2;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class Factorial2 {

    /**
     * @param args the command line arguments
     */
    static int recursiveFact(int number) {
       if(number == 0 || number == 1) {
           return 1;
       }        return number*recursiveFact(number-1);
   }    /*
   recursiveFact(5)
       5*recursiveFact(4)
           5*4*recursiveFact(3)
               5*4*3*recursiveFact(2)
                   5*4*3*2*recursiveFact(1)
                       5*4*3*2*1
   */
    public static void main(String[] args) {
 
        System.out.println(recursiveFact(5));
    }

}
