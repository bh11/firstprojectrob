<%-- 
    Document   : filteredEmployees
    Created on : 2020. febr. 5., 19:23:00
    Author     : Leo
--%>

<%@page import="hu.braininghub.employee.service.dto.EmployeesDto"%>
<%@page import="java.util.List"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Filtered Employees</title>
    </head>
    <body>
        <h1>Filtered Employees</h1>
        <table border="3px">
            <c:forEach var="emp" items="${employees}">
                <tr 
                    <c:if test="${emp.getSalary()>10000}">
                        style="background-color: red"
                    </c:if>
                    >
                    <td><c:out value="${emp.getFirstName()}"/></td>
                    <td><c:out value="${emp.getLastName()}"/></td>
                    <td><c:out value="${emp.getJobHistoryList().size()}"/></td>
                </tr>  
            </c:forEach>
        </table>
    </body>
</html>
