package hu.braininghub.employee.service;

import hu.braininghub.employee.repository.EmployeeDao;
import hu.braininghub.employee.repository.entity.Employees;
import hu.braininghub.employee.service.dto.EmployeesDto;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.beanutils.BeanUtils;

@Stateless
public class EmployeeService {

    @Inject
    private EmployeeDao dao;

    public List<String> getNames()  {
        return dao.getEmployees().stream()
                .map(Employees::getFirstName)
                .collect(Collectors.toList());

    }

    public List<EmployeesDto> getEmployees(String str) {

        List<EmployeesDto> result = new ArrayList<>();

        for (Employees e : dao.findByName(str)) {
            EmployeesDto dto = new EmployeesDto();

            

            try {
                BeanUtils.copyProperties(dto, e);
                result.add(dto);
            } catch (IllegalAccessException | InvocationTargetException ex) {

                Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return result;
    }

    /*private EmployeesDto convert(Employees e) {
        EmployeesDto dto = new EmployeesDto();
        dto.setEmail(e.getEmail());
        dto.setCommissionPct(e.getCommissionPct());
        //...
        return dto;
    }

    private Employees convert(EmployeesDto dto) {
        Employees e = new Employees();
        e.setEmail(dto.getEmail());
        e.setCommissionPct(dto.getCommissionPct());
        //...
        return e;
    }*/
}
