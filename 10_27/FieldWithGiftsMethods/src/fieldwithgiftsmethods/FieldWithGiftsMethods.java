/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fieldwithgiftsmethods;

/**
 *
 * @author Leo
 */
public class FieldWithGiftsMethods {
    
     static void printField(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
    }

    static int generateRandomNumber(int to) {
        /*int randomNumber = (int) (Math.random() * to);
        return randomNumber;*/

        return (int) (Math.random() * to);
    }

    static void putGifts(int[][] field, int giftIcon, int cnt) {
        for (int i = 0; i < cnt; i++) {
            int x = generateRandomNumber(field.length);
            int y = generateRandomNumber(field[i].length);

            if (field[x][y] != giftIcon) {
                field[x][y] = giftIcon;

            } else {
                i--;

            }
        }
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        int gift = 1;
        int row = 8;
        int col = 9;
        int numberOfGifts = 7;
        int[][] field = new int[row][col];

        putGifts(field, gift, numberOfGifts);
        printField(field);


    }
    
}
