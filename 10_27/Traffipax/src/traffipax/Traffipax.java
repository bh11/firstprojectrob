/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package traffipax;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class Traffipax {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int velocityA, velocityB, velocityC;
        int index = 0;
        int[] traffiArray = new int[24];

        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Első sebesség: ");
            velocityA = sc.nextInt();
            System.out.println("Következő sebesség: ");
            velocityB = sc.nextInt();
            System.out.println("Utolsó sebesség: ");
            velocityC = sc.nextInt();

            if (velocityA == 0 || velocityB == 0 || velocityC == 0) {
                traffiArray[index] = 0;
                index++;
            }

            if (velocityB > velocityA && velocityB < velocityC) {
                traffiArray[index] = velocityB;
                index++;
            } else if (velocityA > velocityB && velocityA > velocityC) {
                traffiArray[index] = velocityA;
                index++;
            } else if (velocityC > velocityA && velocityC > velocityB) {
                traffiArray[index] = velocityC;
                index++;
            }

        } while (index <= (traffiArray.length)-1);
        
        System.out.println("Sebességek: ");
        for (int i = 0; i < traffiArray.length; i++) {
            System.out.println(i+ ". óra: "+ traffiArray[i] + ",");
        }
        System.out.println();

    }

}
