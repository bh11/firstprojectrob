/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numbersdividedthreeandaverage;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class NumbersDividedThreeAndAverage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int number = 0;
        boolean correct = false;
        int counter = 1;
        int index = 0;
        int threeDividers = 0;
        int sum = 0;
        int threeWithoutArray = 0;
        int sumWithoutArray = 0;
        int[] numberArray = new int[10];

        Scanner sc = new Scanner(System.in);
        System.out.println("10 db pozitív szám: ");

        do {
            do {
                if (sc.hasNextInt()) {
                    number = sc.nextInt();
                        correct = true;
                } else {
                    sc.next();
                }
            } while (!correct);

            if (number % 3 == 0) {
                threeWithoutArray++;
            }
            sumWithoutArray += number;

            numberArray[index] = number;
            index++;
            counter++;

        } while (counter <= 10);

        for (int i = 0; i < numberArray.length; i++) {
            if (numberArray[i] % 3 == 0) {
                threeDividers++;
            }
            sum += numberArray[i];
        }
        System.out.println("A 3-mal osztható elemek darabszáma: " + threeDividers);
        System.out.println("A számok átlaga: " + (double) sum / numberArray.length);

        System.out.println("3-mal osztható elemek darabszáma (tömb nélkül)" + threeWithoutArray);
        System.out.println("Számok átlaga (tömb nélkül)" + (double) sumWithoutArray / 10);
    }

}
