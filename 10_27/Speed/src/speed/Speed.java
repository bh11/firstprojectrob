/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package speed;

import java.util.Scanner;

/**
 *
 * @author czirjak_zoltan
 */
public class Speed {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int hour = -1, min = hour, speed = hour;
        
        int[] velocities = new int[24];
        Scanner scanner = new Scanner(System.in);
        
        //velocities[30] = 10; --> java.lang.ArrayIndexOutOfBoundsException
        
        do {
            System.out.println("Adja meg az időt és a sebességet szóközzel elválasztva:");
            hour = scanner.nextInt();
            min = scanner.nextInt();
            speed = scanner.nextInt();
            
            if (hour >= 0 && hour < 24 && speed > 130 && velocities[hour] < speed) {
                velocities[hour] = speed;
            }
        
        } while (hour != 0 || min != 0 || speed != 0);
        
        for (int i = 0; i < velocities.length; i++) {
            if (velocities[i] != 0) {
                System.out.printf("%d:00 - %d:59 --> %d km/h\n", i, i, velocities[i]);
            }
        }
        
    }
    
}
