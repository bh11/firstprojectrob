/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentonstage;

/**
 *
 * @author Leo
 */
public class PresentOnStage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int gift = 1;
        int row = 8;
        int col = 9;
        int numberOfGifts = 7;
        
        int[][] field = new int[row][col];
        
        for (int i = 0; i < numberOfGifts; i++) {
            int x = (int)(Math.random() * row);
            int y = (int)(Math.random() * col);
            
            if (field[x][y] != gift) {
                field[x][y] = gift;
                
            } else {
                i--;  
            }
        }
        
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j]);
            }
            System.out.println();
        }
    }
    
}
