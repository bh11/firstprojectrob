/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sentencesandwords;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class SentencesAndWords {

    /**
     * @param args the command line arguments
     */
    
   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String longestWord = "";
        int number = -1;
        String currentWord;
        int cnt = 0;

        do {
            currentWord = scanner.next();
            if (!"exit".equals(currentWord) && currentWord.length() > longestWord.length()) {

                longestWord = currentWord;
                number = cnt;
            }
            cnt++;

        } while (!"exit".equals(currentWord));

        System.out.println(longestWord);
        System.out.println(number);

    }
}
