/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package methods;

/**
 *
 * @author Leo
 */
public class Methods {

    /**
     * @param args the command line arguments
     */
    static boolean sendMail(String to, String text) {

        if (to.contains("@")) {
            System.out.println("Sent. ");
            return true;
        }
        System.out.println("Something went wrong...");
        return false;
    }

    static boolean isEven(int number) {
        if (number % 2 == 0) {
            return true;
        }
        return false;

        // return number % 2 == 0
    }

    static String concat(int numberA, int numberB) {
        //return numberA+ ""+numberB;
        return "" + numberA + numberB;
        //return String.valueOf(numberA) + String.valueOf(numberB);
        //return String.format("what", numberA, numberB);
    }

    static void printName() {
        System.out.println("Leopold Róbert");

    }

    public static void main(String[] args) {

        sendMail("xy@dd.hu", "szöveg");
        sendMail("anotherMailmail.hu", "másik szöveg"); // nincs benne @, false a visszatérési érték

        printName();

        System.out.println(isEven(2));
        System.out.println(isEven(3));

        String s = concat(10, 25);
        System.out.println(s);
        System.out.println(concat(10, 25));
    }

}
