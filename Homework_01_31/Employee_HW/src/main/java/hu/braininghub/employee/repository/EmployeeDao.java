/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employee.repository;

import hu.braininghub.employee.repository.dto.Employee;
import static hu.braininghub.employee.repository.dto.Employee.of;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.sql.DataSource;

/**
 *
 * @author Leo
 */
@Singleton  
public class EmployeeDao {

    @Resource(lookup = "jdbc/lr1")  
    private DataSource ds;

    @PostConstruct  
    public void init() {
        System.out.println("It has been initialized");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("It has been destroyed");
    }

    public List<String> getNames() throws SQLException {
        List<String> names = new ArrayList<>();

        try (Statement stm = ds.getConnection().createStatement()) {
            String sql = "Select * from employees";

            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                names.add(rs.getString("first_name"));
            }
        }
      return names;
   }

    public List<String> searchForNames(String str) throws SQLException {
        List<String> collectedNames = new ArrayList<>();

        try (Statement stm = ds.getConnection().createStatement()) {
            String sql = "Select * from employees where first_name like '%" + str +"%'";

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                collectedNames.add(rs.getString("first_name"));
            }
        }
        return collectedNames;
    }
}
