/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employee.servlet;

import hu.braininghub.employee.service.EmployeeService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Leo
 */
@WebServlet(name = "SearchForEmployeeServlet", urlPatterns = {"/SearchForEmployeeServlet"})
public class SearchForEmployeeServlet extends HttpServlet {

    @Inject
    EmployeeService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<String> searchedNames = service.searchForNames(request.getParameter("search"));
  
        request.setAttribute("names", searchedNames);
        request.getRequestDispatcher("WEB-INF/empnames.jsp").forward(request, response);

    }

}
