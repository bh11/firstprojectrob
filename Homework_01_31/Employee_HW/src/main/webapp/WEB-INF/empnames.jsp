<%-- 
    Document   : empnames
    Created on : 2020. jan. 31., 20:52:28
    Author     : Leo
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employee List</title>
    </head>
    <body>
        <table border = "1">
            <%
                List<String> names = (List<String>) request.getAttribute("names");
                for (String s : names) {
                    out.print("<tr><td>" + s + "</td></tr>");
                }
            %>

        </table>
        <p>
        <form method="get" action="SearchForEmployeeServlet">
            <fieldset>
                <legend>Employee searching </legend>
                <p>Name of employee: <input type="text" name="search" />  </p> 
                <input type="submit" value="Search"/>
            </fieldset>
        </form>
    </body>
</html>
