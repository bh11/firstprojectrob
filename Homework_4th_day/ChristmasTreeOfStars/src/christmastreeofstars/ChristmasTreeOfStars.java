/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package christmastreeofstars;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class ChristmasTreeOfStars {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int number = 0;
        boolean correct = false;
        Scanner sc = new Scanner(System.in);

        while (number <= 0) {
            do {
                System.out.println("Írj be egy pozitív egész számot!");
                if (sc.hasNextInt()) {
                    number = sc.nextInt();
                    correct = true;
                } else {
                    sc.next();
                }
            } while (!correct);
        }
        if (number % 2 == 0) {
            number -= 1;

            for (int i = 0; i < number; i++) {
                for (int j = 0; j < number - i; j++) {
                    System.out.print(" ");
                }
                for (int k = 0; k < (2 * i + 1); k++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        } else {
            for (int i = 0; i < number; i++) {
                for (int j = 0; j < number - i; j++) {
                    System.out.print(" ");
                }
                for (int k = 0; k < (2 * i + 1); k++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        }
    }
}
