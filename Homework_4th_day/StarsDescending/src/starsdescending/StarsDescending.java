/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package starsdescending;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class StarsDescending {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int number = 0;
        boolean correct = false;
        Scanner sc = new Scanner(System.in);

        while (number <= 0) {
            do {
                System.out.println("Írj be egy pozitív egész számot!");
                if (sc.hasNextInt()) {
                    number = sc.nextInt();
                    correct = true;
                } else {
                    sc.next();
                }
            } while (!correct);
        }
        for (int i = 0; i <= number; i++) {
            for (int j = number; j > i; j--) {
                System.out.print("*");
            }
            System.out.println();
        }

    }

}
