/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrayoffifty;

/**
 *
 * @author Leo
 */
public class ArrayOfFifty {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int[] arrayOfFifty = new int[50];
        int minimum = Integer.MAX_VALUE;
        int maximum = Integer.MIN_VALUE;
        int indexMax = 0;
        int indexMin = 0;
        int sum = 0;
        double average;

        for (int i = 0; i < arrayOfFifty.length; i++) {
            arrayOfFifty[i] = (int) ((Math.random() * 201) - 101);
        }
        for (int i = 0; i < arrayOfFifty.length; i++) {
            System.out.print(arrayOfFifty[i] + ",");
            sum += arrayOfFifty[i];

            if (arrayOfFifty[i] > maximum) {
                maximum = arrayOfFifty[i];
                indexMax = i;
            }
            if (arrayOfFifty[i] < minimum) {
                minimum = arrayOfFifty[i];
                indexMin = i;
            }
        }
        average = (1.0 * sum) / arrayOfFifty.length;

        System.out.println("\nMaximum: " + maximum);
        System.out.println("Indexe: " + indexMax);
        System.out.println("Minimum: " + minimum);
        System.out.println("Indexe: " + indexMin);
        System.out.println("Összeg: " + sum);
        System.out.println("Átlag: " + average);

    }
}
