/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decimaltobinary;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class DecimalToBinary {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int number = 0;
        boolean correct = false;
        int[] binaryStorage = new int[50];
        int index = 0;
        Scanner sc = new Scanner(System.in);

        while (number <= 0) {
            do {
                System.out.println("Írj be egy pozitív egész számot!");
                if (sc.hasNextInt()) {
                    number = sc.nextInt();
                    correct = true;
                } else {
                    sc.next();
                }
            } while (!correct);
        }
        int numberForChange = number;
        while (numberForChange > 0) {
            binaryStorage[index] = numberForChange % 2;
            numberForChange = numberForChange / 2;
            index++;
        }
        System.out.println(number + " decimális szám binárisra váltva: ");
        for (int i = index - 1; i >= 0; i--) {
            System.out.print(binaryStorage[i]);
        }
        System.out.println();

    }
}
