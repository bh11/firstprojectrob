/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringstore;

import static java.lang.Thread.currentThread;

/**
 *
 * @author Leo
 */
public class Consumer extends Thread {
    private final StringStore stringStore;
   
    
    public Consumer(StringStore stringStore) {
        this.stringStore=stringStore;
    }
    @Override
    public void run() {
        while(true) {
            String s="Consumer: "+System.currentTimeMillis()+" "+ currentThread().getId()+ " ";
            //stringStore.add(s);
            System.out.println(s);
        }
    }
}
