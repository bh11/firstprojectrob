/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loremipsum;

/**
 *
 * @author Leo
 */
public class LoremIpsum {

    public static void main(String[] args) {
        String str = " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        StringBuilder sb = new StringBuilder(str);
        
        int counterForSentences=0;
        int numberOfIts=0;
        int countOfSpaces=1;
        String secondWords="";
        
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i)=='.' || str.charAt(i)=='?' || str.charAt(i)=='!') {
                counterForSentences++;
            }
        }
        
        for (int i = 0; i < str.length(); i++) {
            if (str.contains("it")) {
                numberOfIts++;
            }
        }
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i)==' ') {
                if (countOfSpaces%2!=0) {
//                    secondWords +=" "+ (str.substring((i + 1), str.indexOf(" ", (i + 1))));
                }
            }
        }
        
        
        System.out.println("Mondatok száma: " +counterForSentences);
        System.out.println("It-ek száma: " + numberOfIts);
        System.out.println("Első és utolsó 'it' közötti rész: \n" + str.substring(str.indexOf("it") + 2, str.lastIndexOf("it")));
        System.out.println("Minden második szó: "+secondWords);
        System.out.println("Kis 'a'-k nagyra cserélése: " +str.replace('a', 'A'));
        System.out.println("Szöveg visszafelé:" + sb.reverse());

    }

}
