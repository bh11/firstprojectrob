/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workersaccountingbills;

/**
 *
 * @author Leo
 */
public class Workers {
    private String name;
    private String systemId;
    private Account[] accounts=new Account[2]; 
    
    public Workers (String name, String systemId, Account[] accounts) {
        this.name=name;
        this.systemId=systemId;
        this.accounts=accounts;
    }
    
    public Account[] getAccounts() {
        return this.accounts;
    }
    
    public void printBills (){
        for (int i = 0; i < this.accounts.length; i++) {
            System.out.println(this.accounts[i].getBillsData());
        }
    }
    public void increaseAllBills () {
        for (int i = 0; i < this.accounts.length; i++) {
            this.accounts[i].inreaseAllBills(1.1);
        }
    }
    public void decreaseAllBills() {
        for (int i = 0; i < this.accounts.length; i++) {
            this.accounts[i].decreaseAllBills(0.1);
        }
    }
    
    
   
    
}
 