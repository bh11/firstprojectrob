/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workersaccountingbills;

import sun.security.x509.SerialNumber;

/**
 *
 * @author Leo
 */
public class Bills {

    private double amount;
    private String serialNumber;
    private static final int AMOUNT_DEFAULT_VALUE = 1000;

    public Bills(int amount, String serialNumber) {
        this.amount = amount;
        this.serialNumber = serialNumber;
    }

    public Bills(String serialNumber) {
        this(AMOUNT_DEFAULT_VALUE,serialNumber);
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public String getSerialNumber() {
        return serialNumber;
    }
    
    public void inreaseBillByTenPercent (double perc) {
        this.setAmount(this.amount*perc);
    }
    
    public void decreaseBillByTenPercent (double perc) {
        this.setAmount(this.amount-(this.amount*perc));
    }

    
}
