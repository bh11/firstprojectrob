/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workersaccountingbills;

/**
 *
 * @author Leo
 */
public class Account {
    private String id;
    private String country;
    private Bills[] bill=new Bills [10];
    private int billIndex;

    public Account(String id, String country) {
        this.id = id;
        this.country = country;
        
    }
    public String getBillsData () {
        String str="";
        for (int i = 0; i < billIndex; i++) {
            str+=this.bill[i].getSerialNumber()+"-"+this.bill[i].getAmount()+"\n";
        }
        return str;
    }
    public void inreaseAllBills (double percent) {
        for (int i = 0; i < billIndex; i++) {
            this.bill[i].inreaseBillByTenPercent(percent);
        }
    }
    
    public void decreaseAllBills (double percent) {
        for (int i = 0; i < billIndex; i++) {
            this.bill[i].decreaseBillByTenPercent(percent);
        }
    }
    
    public void setBills (Bills[] bill) {
        this.bill=bill;
    }
    
    public void addNewBill (Bills bill) {
        this.bill[billIndex]=bill;
        billIndex++;
    }
}
