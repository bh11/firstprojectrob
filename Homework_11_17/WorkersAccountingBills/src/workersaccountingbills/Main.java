/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workersaccountingbills;

/**
 *
 * @author Leo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Bills b1=new Bills(150, "001");
        Bills b2=new Bills(200,"002");
        Bills b3=new Bills("003");
        
        Account acc1=new Account("01A", "hu");
        Account acc2=new Account("02B", "us");
        
        acc1.addNewBill(b1);
        acc1.addNewBill(b2);
        acc2.addNewBill(b3);
        
        Workers w1=new Workers("Béla", "C312", new Account[] {acc1,acc2});
        
        w1.printBills();
        w1.increaseAllBills();
        w1.printBills();
        
        w1.decreaseAllBills();
        w1.printBills();
    }
    
}
