/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise8_everysecondelementbackward;

/**
 *
 * @author Leo
 */
public class Exercise8_everySecondElementBackward {

    /**
     * @param args the command line arguments
     */
    
    static void arrayPrinterBack (int[] arr) {
        for (int i = (arr.length)-1; i >=0; i=i-2) {
            System.out.print(arr[i]+",");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        int[] array = {2,5,8,12,45,98,3,14};
        int[] array2= {5,8,94,34,67,23,3};
        
        arrayPrinterBack(array);
        arrayPrinterBack(array2);
        
    }
    
}
