/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise14_rectangleareamethod;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class Exercise14_RectangleAreaMethod {

    /**
     * @param args the command line arguments
     */
    static int rectangleArea(int a, int b) {

        return a * b;
    }

    static int sideInput(String str) {
        Scanner sc = new Scanner(System.in);
        int side = 0;
        boolean correct = false;
        do {
            System.out.println(str);
            if (sc.hasNextInt()) {
                side = sc.nextInt();
                correct = true;
            } else {
                sc.next();
            }
        } while (!correct);
        return side;
    }

    public static void main(String[] args) {

        int aSide = sideInput("A: ");
        int bSide = sideInput("B: ");

        System.out.println("A téglalap területe: " + rectangleArea(aSide, bSide));

    }
}
