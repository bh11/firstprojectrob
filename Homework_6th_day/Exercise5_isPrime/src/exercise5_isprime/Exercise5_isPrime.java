/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise5_isprime;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class Exercise5_isPrime {

    /**
     * @param args the command line arguments
     */
    static boolean isPrime(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    static int numberInput(String str) {

        Scanner sc = new Scanner(System.in);
        int num = 0;
        boolean correct = false;
        do {
            System.out.println(str);
            if (sc.hasNextInt()) {
                num = sc.nextInt();
                correct = true;
            } else {
                sc.next();
            }
        } while (!correct);
        return num;
    }

    public static void main(String[] args) {

        int number = numberInput("Szám: ");

        if (isPrime(number)) {
            int primeCounter = 0;
            boolean primeSwitch = true;
            for (int i = 2; i <= number * 10; i++) {
                primeSwitch = true;
                for (int j = 2; j <= Math.sqrt(i); j++) {
                    if (i % j == 0) {
                        primeSwitch = false;
                        break;
                    }
                }
                if (primeSwitch == true) {
                    primeCounter++;
                    if (primeCounter == number) {
                        System.out.println(number + ". prímszám: " + i);
                        break;
                    }
                }
            }
        } else {
            System.out.println("A megadott szám nem prímszám!");
        }
    }

}
