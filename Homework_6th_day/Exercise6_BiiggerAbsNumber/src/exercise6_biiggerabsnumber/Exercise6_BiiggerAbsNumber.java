/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise6_biiggerabsnumber;

/**
 *
 * @author Leo
 */
public class Exercise6_BiiggerAbsNumber {

    /**
     * @param args the command line arguments
     */
    
    static int biggerNumber (int a, int b) {
        if (a<0) {
            a *=(-1);
        } else if (b<0) {
            b*=(-1);
        }
        if (a>b) {
            return a;
        } else if(b>a) {
            return b;
        } else {
            return -1;
        }
    }
    public static void main(String[] args) {
        
       
        System.out.println(biggerNumber(8, -8));
        System.out.println(biggerNumber(1, 3));
        System.out.println(biggerNumber(-1, 3));
        System.out.println(biggerNumber(1, -3));
        System.out.println(biggerNumber(5, 2));
        System.out.println(biggerNumber(-5, 2));
        System.out.println(biggerNumber(5, -2));
    }
    
}
