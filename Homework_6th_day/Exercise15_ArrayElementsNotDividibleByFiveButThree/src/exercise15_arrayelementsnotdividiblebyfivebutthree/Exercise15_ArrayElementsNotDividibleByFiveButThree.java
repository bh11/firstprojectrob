/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise15_arrayelementsnotdividiblebyfivebutthree;

/**
 *
 * @author Leo
 */
public class Exercise15_ArrayElementsNotDividibleByFiveButThree {

    /**
     * @param args the command line arguments
     */
    static int elementSelector(int[] arr) {
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 3 == 0 && arr[i] % 5 != 0) {
                counter++;
            }
        }
        return counter;
    }

    public static void main(String[] args) {
        int[] array = {3, 6, 9, 1, 2, 15, 30, 33, 60};

        System.out.println("A tömb 3-mal osztható, de 5-tel nem osztható elemeinek száma: "
                + elementSelector(array));
    }

}
