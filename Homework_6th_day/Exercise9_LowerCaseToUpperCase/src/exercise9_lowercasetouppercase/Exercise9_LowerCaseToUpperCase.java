/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise9_lowercasetouppercase;

/**
 *
 * @author Leo
 */
public class Exercise9_LowerCaseToUpperCase {

    /**
     * @param args the command line arguments
     */
    static void changeChar(char c) {
        if (Character.isUpperCase(c)) {
            System.out.println("A(z) " + c + " karakter kisbejűje: " + Character.toLowerCase(c));
        } else {
            System.out.println("A(z) " + c + " karakter nagybetűje: " + Character.toUpperCase(c));
        }
    }

    static void changeAscii(char c) {
        int convert;
        if ((int) c >= 97 && (int) c <= 122) {
            convert = (int) c - 32;
            System.out.println(c + " nagybetűje: " + (char) convert);
        } else if ((int) c >= 65 && (int) c <= 90) {
            convert = (int) c + 32;
            System.out.println(c + " kisbetűje: " + (char) convert);
        } //else {
//            switch ((int) c) {
//                case 201:
//                    System.out.println(c + " kisbetűje: é");
//                    break;
//                case 252:
//                    System.out.println(c + " nagybetűje: Ü");
//                    break;
//                case 233:
//                    System.out.println(c + " nagybetűje: É");
//                    break;
//                case 336:
//                    System.out.println(c + " kisbetűje: ő");
//                    break;
//                case 337:
//                    System.out.println(c + " nagybetűje: Ő");
//                    break;
//                case 205:
//                    System.out.println(c + " kisbetűje: í");
//                    break;
//                case 246:
//                    System.out.println(c + " nagybetűje: Ö");
//                    break;
//                case 214:
//                    System.out.println(c + " kisbetűje: ö");
//                    break;
//                case 220:
//                    System.out.println(c + " kisbetűje: ü");
//                    break;
//                case 225:
//                    System.out.println(c + " nagybetűje: Á");
//                    break;
//                case 237:
//                    System.out.println(c + " nagybetűje: Í");
//                    break;
//                case 243:
//                    System.out.println(c + " nagybetűje: Ó");
//                    break;
//                case 250:
//                    System.out.println(c + " nagybetűje: Ú");
//                    break;
//                case 193:
//                    System.out.println(c + " kisbetűje: á");
//                    break;
//                case 211:
//                    System.out.println(c + " kisbetűje: ó");
//                    break;
//                case 218:
//                    System.out.println(c + " kisbetűje: ú");
//                    break;
//                case 368:
//                    System.out.println(c + " kisbetűje: ű");
//                    break;
//                case 369:
//                    System.out.println(c + " nagybetűje: Ű");
//                    break;
//                default:
//                    System.out.println(c + " nem a magyar ábécé karaktere!");
//            }
//        }
    }

    public static void main(String[] args) {

        changeChar('g');
        changeChar('C');

        System.out.println();

        changeAscii('a');
        changeAscii('C');
        changeAscii('t');
        changeAscii('X');
        
    }

}
