/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializationexample;

import java.io.Serializable;

/**
 *
 * @author Leo
 */
public class Employee implements Serializable{
        
    private int Id;
    private String name;
    private int age;
    private int salary;
    private Department department;

    public Employee(String name, int age, int salary, Department department) {
        this.Id = IdGen();
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.department = department;
    }

    public int IdGen(){
        return (int)(Math.random()*10000);
    }
    
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" + "Id=" + Id + ", name=" + name + ", age=" + age + ", salary=" + salary + ", department=" + department + '}';
    }    
}
