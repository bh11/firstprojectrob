/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializationexample;



/**
 *
 * @author Leo
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

       
        Employee emp1 = new Employee("Emp1", 32, 1000000, new Department("kis u.", "IT"));
        Employee emp2 = new Employee("Emp2", 33, 764568, new Department("barack u.", "Logistic"));
        Employee emp3 = new Employee("Emp3", 54, 543443, new Department("meggy u.", "Finance"));
        Employee emp4 = new Employee("Emp4", 33, 543443, new Department("szilva u.", "HR"));
        Employee emp5 = new Employee("Emp5", 42, 378954, new Department("dinnye utca", "Support"));

        EmployeeRepository.addEmployee(emp1);
        EmployeeRepository.addEmployee(emp2);
        EmployeeRepository.addEmployee(emp3);
        EmployeeRepository.addEmployee(emp4);
        EmployeeRepository.addEmployee(emp5);

        System.out.println("Employees listed:");
        EmployeeRepository.listEmployees();
        System.out.println("*************************");

        System.out.println("After deleting employee emp2:");
        EmployeeRepository.deleteEmployee(emp2);
        EmployeeRepository.listEmployees();
        System.out.println("***************************");

        System.out.println("Get employee by name (Emp3): ");
        EmployeeRepository.getEmployeeByName("Emp3");
        System.out.println("****************************");

        System.out.println("List by departments:");
        EmployeeRepository.listByDepartments();
        System.out.println("*****************************");
    }
}
