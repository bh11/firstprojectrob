/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializationexample;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Leo
 */

//HF: Képes legyen employeekat tárolni, lehessen hozzáadni/törölni/updatelni/listázni/valamilyen azonosító alapján elkérni egy employeet
//tudjon department szerint listázni (perzisztens legyen)

public class EmployeeRepository{
    private static final String FILENAME = "employee.ser";    
    private static List<Employee> employees = new ArrayList<>() ;
    
    
    public static void serializeEmployee(Set emp){
        try (FileOutputStream fs = new FileOutputStream(FILENAME)){
            ObjectOutputStream ou = new ObjectOutputStream(fs);
            ou.writeObject(emp);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        }
    }
    
    public static List<Employee> deserializeEmployee(){
        List<Employee> emp = null;
        try (FileInputStream fs = new FileInputStream(FILENAME)){
            ObjectInputStream oi = new ObjectInputStream(fs);
            emp = (ArrayList<Employee>) oi.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(EmployeeRepository.class.getName()).log(Level.SEVERE,null, ex);
        }
        return emp;
    }
    
     public static void addEmployee(Employee emp){
        employees.add(emp);
    }
    
    public static void deleteEmployee(Employee emp){
        employees.remove(emp);
    }
    
    public static void listEmployees(){
        employees.stream()
                .forEach((emp)->System.out.println(emp));
    }
    
    public static void getEmployeeByName(String name){
        employees.stream()
                .filter(e-> name.equals(e.getName()))
                .forEach(System.out::println);
    }
    
    public static void listByDepartments(){
        employees.stream()
                .map(e -> e.getDepartment())
                .forEach(System.out::println);
    }

    public static List<Employee> getEmployees() {
        return employees;
    }

    public static void setEmployees(List<Employee> employees) {
        EmployeeRepository.employees = employees;
    }

    
}

