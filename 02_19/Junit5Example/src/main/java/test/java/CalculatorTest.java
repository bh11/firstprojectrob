/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java;

import hu.braininghub.junit5example.java.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Leo
 */
public class CalculatorTest {
      
    @Test
    void testAdd() {
        //Given
        int op1 = 10;
        int op2 = 15;
        Calculator underTest = new Calculator(op1, op2);
        
        //When
        int result = underTest.add();
        
        //Then
        Assertions.assertEquals(op1 + op2 + 1, result, "Wrong addition");
    }
    
     @Test
    void testsubstract(){
        int operndus1 = 10;
        int operandus2 = 15;
        Calculator underTest = new Calculator(operndus1, operandus2);
        
        int result = underTest.substract();
        
        Assertions.assertEquals(operndus1 - operandus2, result, () -> "Wrong substract");
    }
    
    @Test
    void testmultiply(){
        int operndus1 = 10;
        int operandus2 = 15;
        Calculator underTest = new Calculator(operndus1, operandus2);
        
        int result = underTest.multiply();
        
        Assertions.assertEquals(operndus1 * operandus2, result, () -> "Wrong multiply");
    }

}
