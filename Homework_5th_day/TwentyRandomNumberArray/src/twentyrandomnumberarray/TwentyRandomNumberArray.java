/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twentyrandomnumberarray;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class TwentyRandomNumberArray {

    /**
     * @param args the command line arguments
     */
    static int[] intInputNumberTimes(String str, int number) {
        int[] array = new int[number];
        Scanner sc = new Scanner(System.in);
        System.out.println(str);
        for (int i = 0; i < number; i++) {
            if (sc.hasNextInt()) {
                int numberA = sc.nextInt();
                if (numberA > 0) {
                    array[i] = numberA;
                } else {
                    i--;
                }
            } else {
                sc.next();
                i--;
            }
        }
        return array;
    }

    static int numberInput(String str) {
        Scanner sc = new Scanner(System.in);
        boolean correct = false;
        int number = 0;
        do {
            System.out.println(str);
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                if (number > 0) {
                    correct = true;
                }

            } else {
                sc.next();
            }
        } while (!correct);
        return number;
    }

    static int numberIndex(int[] arr, int num) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == num) {
                index = i;
            }
        }
        return index;
    }

    public static void main(String[] args) {

        int[] numberArray = intInputNumberTimes("írj be 20 pozitív egész számot", 20);

        int number = numberInput("Írj be egy pozitív egész számot!");
        int ind = numberIndex(numberArray, number);

        if (ind != -1) {
            System.out.println("A megadott szám első indexe: " + ind);
        } else {
            System.out.println("A megadott szám nincs a tömbben!");
        }
    }
}
