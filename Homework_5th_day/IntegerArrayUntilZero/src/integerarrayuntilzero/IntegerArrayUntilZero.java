/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integerarrayuntilzero;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class IntegerArrayUntilZero {

    /**
     * @param args the command line arguments
     */
    static void printArrayToIndex(int[] arr, int ind) {

        for (int i = 0; i < ind; i++) {
            System.out.print(arr[i] + ",");
        }
        System.out.println();
    }

    static void reversedArrayToIndex(int[] arr, int ind) {

        for (int i = (ind - 1); i >= 0; i--) {
            System.out.print(arr[i] + ",");
        }
        System.out.println();
    }

    static void minElementOfArray(int[] arr, int ind) {
        int min = Integer.MAX_VALUE;
        int index = 0;

        for (int i = 0; i < ind; i++) {
            if (min > arr[i]) {
                min = arr[i];
                index = i;
            }
        }
        System.out.println("A tömb legkisebb eleme:" + min);
        System.out.println("Indexe:" + index);
    }

    static void maxElementOfArray(int[] arr, int ind) {
        int max = Integer.MIN_VALUE;
        int index = 0;

        for (int i = 0; i < ind; i++) {
            if (max < arr[i]) {
                max = arr[i];
                index = i;
            }
        }
        System.out.println("A tömb legnagyobb eleme:" + max);
        System.out.println("Indexe:" + index);
    }

    public static void main(String[] args) {

        int index = 0;
        int number;
        boolean inputZero = false;
        int maxElementNumber = 100;
        Scanner sc = new Scanner(System.in);

        int[] numberArray = new int[maxElementNumber];

        do {
            System.out.println("Írj be egész számokat 0 végjelig! ");
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                switch (number) {
                    case 0:
                        inputZero = true;
                        break;
                    default:
                        numberArray[index] = number;
                        index++;
                }
            } else {
                sc.next();
            }
        } while (!inputZero);

        System.out.println("A tömb elemei: ");
        printArrayToIndex(numberArray, index);
        minElementOfArray(numberArray, index);
        maxElementOfArray(numberArray, index);
        System.out.println("A tömb elemei fordított sorrendben: ");
        reversedArrayToIndex(numberArray, index);

    }

}
