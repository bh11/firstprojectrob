/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package methodstringcharint;

/**
 *
 * @author Leo
 */
public class MethodStringCharInt {

    static int charCounter(String str, char c) {
        int charCount = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                charCount++;
            }
        }
        return charCount;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(charCounter("Hello World", 'l'));
        System.out.println(charCounter("Honolulu", 'u'));
        System.out.println(charCounter("Three witches watch three Swatch watches", 'w'));
    }

}
