/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fiftyrandomnumberarray;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class FiftyRandomNumberArray {

    /**
     * @param args the command line arguments
     */
    static void printArray(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + ",");
        }
    }

    static int numberInput(String str) {
        int number = 0;
        boolean correct = false;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println(str);
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                if (number >= 0 && number <= 9) {
                    correct = true;
                }
            } else {
                sc.next();
            }
        } while (!correct);

        return number;
    }

    static int sumArrayElements(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }

    static int elementCounter(int[] arr, int nr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == nr) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        int summary;
        int counter;
        int numberFromUser;
        int[] numberArray = new int[50];

        for (int i = 0; i < numberArray.length; i++) {
            numberArray[i] = (int) (Math.random() * 10);
        }
        summary = sumArrayElements(numberArray);

        numberFromUser = numberInput("Írj be egy pozitív egész számot 0 és 9 között!");
        System.out.println();
        counter = elementCounter(numberArray, numberFromUser);

        System.out.println("A tömb elemei: ");
        printArray(numberArray);

        System.out.println("A tömb elemeinek összege: " + summary);

        System.out.println("Számtani közép: " + (double) summary / numberArray.length);

        System.out.println("A megadott szám összesen ennyiszer szerepel a tömbben: " + counter);
    }

}
