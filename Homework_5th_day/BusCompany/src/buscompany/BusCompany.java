/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscompany;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class BusCompany {

    /**
     * @param args the command line arguments
     */
    static double fineTypeDecider(char ch, double fine) {
        double loss = 0.8;

        if (ch == 'c') {
            return (double) fine * loss;
        } else if (ch == 'h') {
            return fine;
        }
        return fine;
    }

    static void HoursMinsFinePrinter(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0) {
                System.out.printf("%d:00-%d:59, %d Ft ", i, i, arr[i]);
                System.out.println();
            }
        }
    }

    public static void main(String[] args) {
        int hour;
        int min;
        double fine;
        char fineType;
        boolean endSign = false;
        Scanner sc = new Scanner(System.in);
        int[] fineStore = new int[24];

        do {
            System.out.println("Írja be a bírságok adatait szóközzel elválasztva (óra perc bírságfajta összeg)!");
            hour = sc.nextInt();
            min = sc.nextInt();
            fineType = sc.next().charAt(0);
            fine = sc.nextDouble();

            fine = fineTypeDecider(fineType, fine);

            if (hour < 24 && hour >= 0 && min <= 59 && min >= 0 && fineType != 'x') {
                fineStore[hour] = fineStore[hour] + (int) fine;
            } else if (hour == 0 && min == 0 && fineType == 'x' && fine == 0) {
                endSign = true;
            }
        } while (!endSign);

        HoursMinsFinePrinter(fineStore);

    }

}
