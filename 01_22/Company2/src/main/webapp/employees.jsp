<%-- 
    Document   : employees
    Created on : 2020.01.22., 17:24:58
    Author     : Leo
--%>

<%@page import="java.util.List"%>
<%@page import="hu.braininghub.company.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% List<Employee> employees = (List<Employee>) request.getAttribute("employees"); %>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Adress</th>
                    <th>Salary</th>
                </tr>
            </thead>
            <tbody>
            <h2>Employee list</h2>
            <% for (int i = 0; i < employees.size(); i++) {%>
            <tr>
                <td><%= employees.get(i).getName()%></td>
                <td><%= employees.get(i).getAddress()%></td>
                <td><%= employees.get(i).getSalary()%></td>
            </tr>
            <% }%>
        </tbody>
    </table>
    <h2>Add new employee</h2>
    <form method = "post" action="AddEmployeeServlet">
        <p>Name: <input type = "text" name="name"/></p>
        <p>Address: <input type = "text" name="address"/></p>
        <p>Salary: <input type = "number" name="salary"/></p>
        <input type= "submit" value = "Add employee"/>
    </form>
    <h2>Delete employee</h2>
    <form method = "post" action="DeleteEmployeeServlet">
        <p>Name: <input type = "text" name="name"/></p>
        <input type= "submit" value = "Delete employee"/>
    </form>          
</body>
</html>

