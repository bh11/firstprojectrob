/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeworkthirdday_quadrantdecider;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class HomeWorkThirdDay_QuadrantDecider {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int numberA=0;
        int numberB=0;
        boolean correct = true;

        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Írj be egy számot!");
            if (sc.hasNextInt()) {
                numberA = sc.nextInt();
                correct = false;
            } else {
                sc.next();
            }

        } while (correct);
        
        correct=true;
        
        do {
            System.out.println("Következő szám: ");
            if (sc.hasNextInt()) {
                numberB = sc.nextInt();
                correct = false;
            } else {
                sc.next();
            }

        } while (correct);

        if (numberA >= 0 && numberB >= 0) {
            System.out.println("1. síknegyed");
        } else if (numberA >= 0 && numberB < 0) {
            System.out.println("4. síknegyed");
        } else if (numberA < 0 && numberB >= 0) {
            System.out.println("2. síknegyed");
        } else if (numberA < 0 && numberB < 0) {
            System.out.println("3. síknegyed");
        }

    }

}
