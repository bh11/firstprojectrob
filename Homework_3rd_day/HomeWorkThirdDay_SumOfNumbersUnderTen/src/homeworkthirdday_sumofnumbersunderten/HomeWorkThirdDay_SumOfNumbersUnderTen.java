/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeworkthirdday_sumofnumbersunderten;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class HomeWorkThirdDay_SumOfNumbersUnderTen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int number = 0;
        int sum = 0;
        boolean correct = true;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Írj be egy egész számot 10 alatt!");
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                correct = false;
            } else {
                sc.next();
            }
        } while (correct);

        if (number >= 10) {
            System.out.println("A beírt szám nem 10 alatt volt!");
        } else {
            sum += number;
        }
        while (number < 10) {
            System.out.println("A következő szám: ");
            number = sc.nextInt();
            sum += number;
        }
        if (sum != 0) {
            System.out.println("A beírt számok összege: " + sum);
        }
    }
}
