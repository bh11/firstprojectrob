/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeworkthirdday_numbersendeleven;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class HomeWorkThirdDay_NumbersEndEleven {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberA = 0;
        int numberB = 0;
        boolean correct = true;

        do {
            System.out.println("Első szám: ");
            if (sc.hasNextInt()) {
                numberA = sc.nextInt();
                correct = false;
            } else {
                sc.next();
            }
        } while (correct);
        correct = true;

        do {
            System.out.println("Második szám: ");
            if (sc.hasNextInt()) {
                numberB = sc.nextInt();
                correct = false;
            } else {
                sc.next();
            }
        } while (correct);

        System.out.println();

        if (numberA < numberB) {
            for (int i = numberA; i <= numberB; i++) {
                if (i % 100 == 11) {
                    System.out.println(i);
                }
            }
        } else if (numberB < numberA) {
            for (int i = numberA; i >= numberB; i--) {
                if (i % 100 == 11) {
                    System.out.println(i);
                }
            }
        } else {
            System.out.println("A két szám egyenlő!");
        }
    }
}
