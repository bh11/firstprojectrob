/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeworkthirdday_evennumbers;

/**
 *
 * @author Leo
 */
public class HomeWorkThirdDay_EvenNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        for (int i = 55; i <= 88; i++) {
            if (i % 2 == 0) {
                System.out.print(i + ",");
            }
        }
        System.out.println();

        int number = 55;
        while (number <= 88) {
            if (number % 2 == 0) {
                System.out.print(number + ",");
            }
            number++;
        }
        System.out.println();
    }
}
