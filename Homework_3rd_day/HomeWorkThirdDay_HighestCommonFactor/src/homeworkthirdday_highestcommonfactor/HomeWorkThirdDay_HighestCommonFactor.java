/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeworkthirdday_highestcommonfactor;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class HomeWorkThirdDay_HighestCommonFactor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberA = 0;
        int numberB = 0;
        boolean correct = true;

        do {
            System.out.println("Írj be egy számot 10 és 500 között!");
            if (sc.hasNextInt()) {
                numberA = sc.nextInt();
                if (numberA >= 10 && numberA <= 500) {
                    correct = false;
                } else {
                    System.out.println("A beírt szám nem 10 és 500 között van!");
                }
            } else {
                sc.next();
            }
        } while (correct);

        correct = true;

        do {
            System.out.println("Következő szám: ");
            if (sc.hasNextInt()) {
                numberB = sc.nextInt();
                if (numberB >= 10 && numberB <= 500) {
                    correct = false;
                } else {
                    System.out.println("A beírt szám nem 10 és 500 között van!");
                }
            } else {
                sc.next();
            }
        } while (correct);

        int remainder = numberA % numberB;
        while (remainder != 0) {
            numberA = numberB;
            numberB = remainder;
            remainder = numberA % numberB;
        }
        System.out.println("A legnagyobb közös osztó:" + numberB);

    }

}
