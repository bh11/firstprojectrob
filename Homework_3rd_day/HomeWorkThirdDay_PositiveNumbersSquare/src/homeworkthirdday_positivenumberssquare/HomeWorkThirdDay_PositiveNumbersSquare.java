/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeworkthirdday_positivenumberssquare;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class HomeWorkThirdDay_PositiveNumbersSquare {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = -1;

        do {
            System.out.println("Írj be egy pozitív egész számot!");
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                for (int i = 0; i <= number; i++) {
                    System.out.println(number + "-ig a számok négyzete: "
                            + (i * i));
                }
            } else {
                sc.next();
            }
        } while (number < 0);
    }

}
