/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HomeWorkThirdDay_EvenDividers;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class HomeWorkThirdDay_EvenDividers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int number = -1;
        int divider;
        boolean correct = true;
        boolean noEvenDivider = true;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Írj be egy egész számot!");
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                correct = false;
            } else {
                sc.next();
            }
        } while (correct);

        if (number > 0) {
            divider = number - 1;

            while (divider != 0) {
                if (number % divider == 0) {
                    if (divider % 2 == 0) {
                        System.out.println(number + " szám páros osztója: " + divider);
                        noEvenDivider = false;
                    }
                }
                divider--;
            }
        } else if (number < 0) {
            divider = number + 1;

            while (divider != 0) {
                if (number % divider == 0) {
                    if (divider % 2 == 0) {
                        System.out.println(number + " szám páros osztója: " + divider);
                        noEvenDivider = false;
                    }
                }
                divider++;
            }
        } else {
            noEvenDivider = true;
        }
        if (noEvenDivider) {
            System.out.println(number + " számnak nincs páros osztója!");
        }
    }
}
