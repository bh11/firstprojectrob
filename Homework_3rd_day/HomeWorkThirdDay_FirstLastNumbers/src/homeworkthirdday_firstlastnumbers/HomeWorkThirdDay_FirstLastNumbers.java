/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeworkthirdday_firstlastnumbers;

/**
 *
 * @author Leo
 */
public class HomeWorkThirdDay_FirstLastNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int firstDigit = 0;
        int lastDigit;
        int generatedNumber = (int) ((Math.random() * 9000) + 1000);
        int number = generatedNumber;
        String intNumberToString = String.valueOf(generatedNumber);

        System.out.println(generatedNumber + " első számjegye: " + intNumberToString.charAt(0));
        System.out.println(generatedNumber + " utolsó számjegye: " + intNumberToString.charAt(3));
        System.out.println();

        while (number > 10) {
            firstDigit = number /= 10;
        }
        System.out.println(generatedNumber + " szám első számjegye: " + firstDigit);

        lastDigit = generatedNumber % 10;
        System.out.println(generatedNumber + " szám utolsó számjegye: " + lastDigit);
    }

}
