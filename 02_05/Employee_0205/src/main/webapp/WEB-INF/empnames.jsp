<%-- 
    Document   : empnames
    Created on : 2020. jan. 31., 20:52:28
    Author     : Leo
--%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employee List</title>
    </head>
    <body>
        <table border = "1">
            <%
                List<String> names = (List<String>) request.getAttribute("names");
                for (String s : names) {
                    out.print("<tr><td>" + s + "</td></tr>");
                }
            %>

        </table>
            <table>
                <c:forEach var="name" items="${names}">
                    <tr><td><c:out value="${name}" /> </td></tr>
                </c:forEach>>
                
            </table> 
        <p>
        <form method="post" action="EmployeeServlet">
            <fieldset>
                <legend>Employee searching </legend>
                <p>Name of employee: <input type="text" name="search" />  </p> 
                <input type="submit" value="Search"/>
            </fieldset>
        </form>
        
    </body>
</html>
