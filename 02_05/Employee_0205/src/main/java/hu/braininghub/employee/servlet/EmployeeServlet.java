/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employee.servlet;

import hu.braininghub.employee.repository.EmployeeDao;
import hu.braininghub.employee.repository.dto.Employee;
import hu.braininghub.employee.service.EmployeeService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Leo
 */
@WebServlet(name = "EmployeeServlet", urlPatterns = {"/EmployeeServlet"})
public class EmployeeServlet extends HttpServlet {

    @Inject
    private EmployeeService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<String> names = service.getNames();

        request.setAttribute("names", names);
        request.getRequestDispatcher("WEB-INF/empnames.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Employee> employees = service.getEmployees(request.getParameter("search"));
      
        request.setAttribute("employees", employees);
        request.getRequestDispatcher("WEB-INF/filteredEmployees.jsp").forward(request, response);
    }
}
