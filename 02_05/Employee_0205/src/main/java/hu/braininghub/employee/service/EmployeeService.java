package hu.braininghub.employee.service;

import hu.braininghub.employee.repository.EmployeeDao;
import hu.braininghub.employee.repository.dto.Employee;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class EmployeeService {
    
    
    @Inject
    private EmployeeDao dao;

    public List<String> getNames() {
        try {
            return dao.getEmployees().stream()
                    .map(Employee::getFirstName)
                    .collect(Collectors.toList());
            
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ArrayList<>();
    }

    public List<Employee> getEmployees(String str) {
        try {
            return dao.findByName(str);
            
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ArrayList<>();
    }
}

