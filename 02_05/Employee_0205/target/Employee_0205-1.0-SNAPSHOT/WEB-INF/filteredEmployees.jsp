<%-- 
    Document   : filteredEmployees
    Created on : 2020. febr. 5., 19:23:00
    Author     : Leo
--%>

<%@page import="hu.braininghub.employee.repository.dto.Employee"%>
<%@page import="java.util.List"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Filtered Employees!</h1>

        <table>
            <%
                List<Employee> employees = (List<Employee>) request.getAttribute("employees");

                for (Employee e : employees) {
                    out.print("<tr><td>" 
                            + e.getFirstName() + "</td><td>"
                            + e.getLastName() + "</td><td>"
                            + e.getSalary() + "</td></tr>");
                }
            %>
        </table>
    </body>

</html>
