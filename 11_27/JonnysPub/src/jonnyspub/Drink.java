/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jonnyspub;

/**
 *
 * @author Leo
 */
public class Drink {
    private int sizeOfDrink;
    private int waterQuantity;
    private int wineQuantity;
    private double alcoholQuantityOfDrink;
    

    public Drink(int sizeOfDrink, int waterQuantity, int wineQuantity) {
        this.sizeOfDrink = sizeOfDrink;
        this.waterQuantity = waterQuantity;
        this.wineQuantity = wineQuantity;
    }

    public int getSizeOfDrink() {
        return sizeOfDrink;
    }

    public void setSizeOfDrink(int sizeOfDrink) {
        this.sizeOfDrink = sizeOfDrink;
    }

    public int getWaterQuantity() {
        return waterQuantity;
    }

    public void setWaterQuantity(int waterQuantity) {
        this.waterQuantity = waterQuantity;
    }

    public int getWineQuantity() {
        return wineQuantity;
    }

    public void setWineQuantity(int wineQuantity) {
        this.wineQuantity = wineQuantity;
    }

    public double getAlcoholQuantityOfDrink() {
        return alcoholQuantityOfDrink;
    }

    public void setAlcoholQuantityOfDrink(double alcoholQuantityOfDrink) {
        this.alcoholQuantityOfDrink = alcoholQuantityOfDrink*0.1;
    }
    
    

    @Override
    public String toString() {
        return "Drink attributes: " + "sizeOfDrink=" + sizeOfDrink + ", waterQuantity=" + waterQuantity + ", wineQuantity=" + wineQuantity;
    }
    
    
}
