/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jonnyspub;

/**
 *
 * @author Leo
 */
public class Pub {

    private int waterAvailable;
    private int wineAvailable;
    private final int MIN_AGE = 18;
    private final int PRICE_OF_WINE = 300;
    private final int PRICE_OF_WATER = 50;
    private final double PERCENTAGE_OF_ALCOHOL = 0.1; 


    public Pub(int waterAvailable, int wineAvailable) {
        this.waterAvailable = waterAvailable;
        this.wineAvailable = wineAvailable;
    }

    public int getWaterAvailable() {
        return waterAvailable;
    }

    public void setWaterAvailable(int waterAvailable) {
        this.waterAvailable = waterAvailable;
    }

    public int getWineAvailable() {
        return wineAvailable;
    }

    public void setWineAvailable(int wineAvailable) {
        this.wineAvailable = wineAvailable;
    }

    @Override
    public String toString() {
        return "Pub attributes: " + "waterAvailable=" + waterAvailable + ", wineAvailable=" + wineAvailable;
    }
    
    

    public void serveDrink(Person person, Drink drink) {
        int paidForWine = PRICE_OF_WINE * drink.getWineQuantity();
        int paidForWater = PRICE_OF_WATER * drink.getWaterQuantity();
        
        if (person.getAge() >= MIN_AGE) {
            if (person.getMoneyAmount() < (paidForWine + paidForWater)) {
                setWaterAvailable(getWaterAvailable() - drink.getWaterQuantity());
                setWineAvailable(getWineAvailable() - drink.getWineQuantity());

                person.setMoneyAmount(person.getMoneyAmount() - (paidForWine + paidForWater));
                person.setAlcoholDrunk(drink.getWineQuantity()* PERCENTAGE_OF_ALCOHOL);

            } else {
                System.out.println("Yo don't have enough money!");
            }

        } else {
            System.out.println("You are under 18 years!");
        }
    }
}
