/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jonnyspub;

/**
 *
 * @author Leo
 */
public class Utils {
    
    public void getDrinkWithLowestAlcohol (Drink[] drink) {
        Drink[] returnedDrink=new Drink[1];
        double alcoholQuantOfCurrentDrink=drink[0].getAlcoholQuantityOfDrink();
        for (int i = 0; i < drink.length; i++) {
            if (drink[i].getAlcoholQuantityOfDrink()<alcoholQuantOfCurrentDrink) {
                alcoholQuantOfCurrentDrink=drink[i].getAlcoholQuantityOfDrink();
                returnedDrink[0]=drink[i];
            }
        }
        System.out.println("The drink with the lowest alcohol: " +returnedDrink.toString());
    }
    
    public void getYoungestPerson (Person[] person) {
        Person[] returnedPerson=new Person[1];
        int currentAge=person[0].getAge();
        
        for (int i = 0; i < person.length; i++) {
            if (person[i].getAge()<currentAge){
                currentAge=person[i].getAge();
                returnedPerson[0]=person[i];
            }
        }
        System.out.println("The youngest person is: " + returnedPerson.toString());
    }
}
