/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jonnyspub;

/**
 *
 * @author Leo
 */
public class Person {
    private int age;
    private int moneyAmount;
    private double alcoholDrunk;

    public Person(int age, int moneyAmount, double alcoholDrunk) {
        this.age = age;
        this.moneyAmount = moneyAmount;
        this.alcoholDrunk = alcoholDrunk;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(int moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public double getAlcoholDrunk() {
        return alcoholDrunk;
    }

    public void setAlcoholDrunk(double alcoholDrunk) {
        this.alcoholDrunk = alcoholDrunk;
    }

    @Override
    public String toString() {
        return "Person attributes: " + "age=" + age + ", moneyAmount=" + moneyAmount + ", alcoholDrunk=" + alcoholDrunk;
    }
    
    
    
}
