/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jonnyspub;

/**
 *
 * @author Leo
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Pub pub=new Pub(100, 100);
        Drink d1=new Drink(20, 40, 40);
        Person p1=new Person(11, 100, 0);
        Person p2=new Person (20, 500, 0);
        
        pub.serveDrink(p1, d1);
        System.out.println(p2.toString());
        pub.serveDrink(p2, d1);
        System.out.println(p2.toString());
        
    }
    
}
