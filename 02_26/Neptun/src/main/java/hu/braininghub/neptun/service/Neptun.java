/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.service;

import hu.braininghub.neptun.dto.StudentDto;
import hu.braininghub.neptun.mapper.StudentMapper;
import hu.braininghub.neptun.repository.StudentDao;
import hu.braininghub.neptun.repository.entity.Student;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.Singleton;
import javax.inject.Inject;
/**
 *
 * @author Leo
 */
@Singleton
public class Neptun {

    @Inject
    private StudentDao dao;

    @Inject
    private StudentMapper mapper;

    public List<StudentDto> getStudents() {
        Iterable<Student> students = dao.findAll();
        
        List<StudentDto> dtos = new ArrayList<>();

        students.forEach(student -> dtos.add(mapper.toDto(student)));
        dtos.forEach(System.out::println);
        return dtos;
    }
    
    public StudentDto getStudentById(int id){
         
        Optional<Student> studentsById = dao.findById(id);
        return mapper.toDto(studentsById.get());
    }


    void setDao(StudentDao dao) {
        this.dao = dao;
    }

    void setMapper(StudentMapper mapper) {
        this.mapper = mapper;
    }
    
    
}
