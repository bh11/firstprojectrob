/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.mapper;

import hu.braininghub.neptun.dto.SubjectDto;
import hu.braininghub.neptun.repository.entity.Subject;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author Leo
 */
@Singleton
@LocalBean
public class SubjectMapper implements Mapper<Subject, SubjectDto> {

    @Inject
    private StudentMapper studentMapper;

    @Override
    public Subject toEntity(SubjectDto dto) {
        Subject subject = new Subject();
        subject.setDescription(dto.getDescription());
        subject.setId(dto.getId());
        subject.setName(dto.getName());

        return subject;
    }

    @Override
    public SubjectDto toDto(Subject entity) {
        SubjectDto subjectDto = new SubjectDto();
        subjectDto.setDescription(entity.getDescription());
        subjectDto.setId(entity.getId());
        subjectDto.setName(entity.getName());

        return subjectDto;
    }

}

