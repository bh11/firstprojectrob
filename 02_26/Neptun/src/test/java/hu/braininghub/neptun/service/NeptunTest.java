/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.service;

import hu.braininghub.neptun.dto.StudentDto;
import hu.braininghub.neptun.mapper.StudentMapper;
import hu.braininghub.neptun.repository.StudentDao;
import hu.braininghub.neptun.repository.entity.Student;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Leo
 */
@ExtendWith(MockitoExtension.class)
public class NeptunTest {
    
    @Mock
    private StudentDao dao;
    
    @Mock
    private StudentMapper mapper;
    
    private Neptun underTest;
    
    @BeforeEach
    void init(){
        underTest=new Neptun();
        underTest.setDao(dao);
        underTest.setMapper(mapper);
    }
    
    @Test
    void testGetStudentsWithEmptyResult(){
        //Given
        when(dao.findAll()).thenReturn(new ArrayList<>());
        
        //When
        List<StudentDto> students = underTest.getStudents();
        
        //Then
        assertEquals(new ArrayList<>(), students);
    }
    
    @Test
    void testGetStudentsWith1Item(){
        //Given
        Student s = mock(Student.class);
        StudentDto dto = mock(StudentDto.class);
        
        when(dao.findAll()).thenReturn(Arrays.asList(s));
        when(mapper.toDto(s)).thenReturn(dto);
        
        //When
        List<StudentDto> students = underTest.getStudents();
        
        //Then
        assertEquals(Arrays.asList(dto), students);
    }
    
    @Test
    void testGetStudentById(){
        //Given
        Student s = Mockito.mock(Student.class);
        StudentDto dto = Mockito.mock(StudentDto.class);
        
        Mockito.when(dao.findById(0)).thenReturn(Optional.of(s));
        Mockito.when(mapper.toDto(s)).thenReturn(dto);  
   
        //When
        StudentDto result = underTest.getStudentById(0);
        
        //Then
        Assertions.assertSame(dto, result);
    }

}
