/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfactory;

/**
 *
 * @author Leo
 */
public class AnimalFactory {

    public static AbstractAnimal create(String type) {
        if ("cat".equalsIgnoreCase(type)) {
            return new Cat();
        } else if ("dog".equalsIgnoreCase(type)) {
            return new Dog();
        } else if ("programmer".equalsIgnoreCase(type)) {
            return new Programmer();
        }
        return null;
    }
}
