/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myfactory;

/**
 *
 * @author Leo
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AbstractAnimal ab=new AnimalFactory().create("cat");
        ab.sayHello();
        AbstractAnimal ab2=new AnimalFactory().create("programmer");
        ab2.sayHello();
        AbstractAnimal ab3=new AnimalFactory().create("dog");
        ab3.sayHello();
    }
    
}
