/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firstsingleton;

/**
 *
 * @author Leo
 */
public class MySingleton {

    private static MySingleton instance;

    private MySingleton() { //példányosítás letiltva

    }

    public static MySingleton getInstance() { //nem szálbiztos

        if (instance == null) {
            instance = new MySingleton();
        }
        return instance;
    }

    public boolean validateAge(int age) {

        if (age < 14) {
            return false;
        }
        return true;
    }
}
