/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mybuilder;

/**
 *
 * @author Leo
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Car c = new Car.CarBuilder("Trabi", "Fehér")
                .setAbs(true)
                .setMaxSpeed(221)
                .build();

        Car2 c2 = new Car2("Trabi", "Fehér")
                .setAbs(true)
                .setMaxSpeed(220);
    }

}
