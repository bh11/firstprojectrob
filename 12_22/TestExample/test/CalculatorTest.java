/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import junit.framework.Assert;
import org.junit.Test;
import testexample.Calculator;

/**
 *
 * @author Leo
 */
public class CalculatorTest {

    public CalculatorTest() {
    }

    @Test
    public void testAdd() {
        //Give
        int number1 = 10;
        int number2 = 25;
        Calculator calc = new Calculator(number1, number2);

        //When
        int result = calc.add();

        //Then
        Assert.assertEquals(number1 + number2, result);

    }

    @Test
    public void testSubstract() {
        //Give
        int number1 = 25;
        int number2 = 10;
        Calculator calc = new Calculator(number1, number2);

        //When
        int result2 = calc.substract();

        //Then
        Assert.assertEquals(number1 - number2, result2);
    }
}
