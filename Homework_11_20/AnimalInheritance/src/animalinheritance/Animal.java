/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalinheritance;

/**
 *
 * @author Leo
 */
public class Animal {

    protected String name;
    protected int age;
    protected int countOfLegs;

    public Animal(String name, int age, int countOfLegs) {
        this.name = name;
        this.age = age;
        this.countOfLegs = countOfLegs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCountOfLegs() {
        return countOfLegs;
    }

    public void setCountOfLegs(int countOfLegs) {
        this.countOfLegs = countOfLegs;
    }

    public void say() {
        System.out.println("Animal is in superclass and says something.");
    }

    @Override
    public String toString() {
        return "Animal's attributes: " + "name=" + name + ", age=" + age + ", countOfLegs=" + countOfLegs;
    }

}
