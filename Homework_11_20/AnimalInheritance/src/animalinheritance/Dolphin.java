/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalinheritance;

/**
 *
 * @author Leo
 */
public class Dolphin extends Animal {

    private boolean hungry;

    public Dolphin(boolean hungry, String name, int age, int countOfLegs) {
        super(name, age, countOfLegs);
        this.hungry = hungry;
    }

    @Override
    public void say() {
        System.out.println("Eeek-eeek!");
    }

    @Override
    public String toString() {
        return "Dolphin's attributes: " + "hungry=" + hungry + "name=" + name + ", age=" + age + ", countOfLegs=" + countOfLegs;
    }

}
