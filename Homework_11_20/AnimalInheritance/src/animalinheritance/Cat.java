/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalinheritance;

/**
 *
 * @author Leo
 */
public class Cat extends Animal {

    private boolean domestic;

    public Cat(boolean domestic, String name, int age, int countOfLegs) {
        super(name, age, countOfLegs);
        this.domestic = domestic;
    }

    public boolean isDomestic() {
        return domestic;
    }

    public void setDomestic(boolean domestic) {
        this.domestic = domestic;
    }

    @Override
    public void say() {
        System.out.println("Meow-meow!");
    }

    @Override
    public String toString() {
        return "Cat's attributes: " + "domestic=" + domestic + "name=" + name + ", age=" + age + ", countOfLegs=" + countOfLegs;
    }

}
