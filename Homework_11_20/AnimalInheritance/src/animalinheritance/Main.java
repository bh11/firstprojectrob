/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalinheritance;

/**
 *
 * @author Leo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    static void animalPrinter(Animal[] anim) {

        for (int i = 0; i < 4; i++) {
            if (anim[i] != null) {
                System.out.println(anim[i]);
                anim[i].say();
            }
        }
    }

    public static void main(String[] args) {
        Animal[] animals = new Animal[10];
        Animal anim = new Animal("Animal Super", 3, 4);
        animals[0] = anim;

        Dog dog = new Dog("Brown", "Buksi", 4, 4);
        animals[1] = dog;

        Cat cat = new Cat(true, "Cirmi", 5, 4);
        animals[2] = cat;

        Dolphin dolphin = new Dolphin(false, "Flipper", 2, 0);
        animals[3] = dolphin;
        
        animalPrinter(animals); 
    }

}
