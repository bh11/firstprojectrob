/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalinheritance;

/**
 *
 * @author Leo
 */
public class Dog extends Animal {

    private String color;

    public Dog(String color, String name, int age, int countOfLegs) {
        super(name, age, countOfLegs);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void say() {
        System.out.println("Woof-woof!");
    }

    @Override
    public String toString() {
        return "Dog's attributes: " + "color=" + color + "name=" + name + ", age=" + age + ", countOfLegs=" + countOfLegs;
    }

}
