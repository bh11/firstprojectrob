/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop.model;

/**
 *
 * @author Toth.Attila
 */
public class Hammer extends Car{
    private int identifier;
    
    public Hammer() {
        this.identifier = Car.identifier;
    }

    @Override
    void test() {
        System.out.println("DO NOTHING");
    }

    @Override
    public int getIdentifier() {
        return identifier;
    }
    
    @Override
    public String toString() {
        return "Hammer car with " + "identifier=" + identifier + ", plate" + this.plate;
    }
    
}
