/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop.model;

import java.util.Objects;

/**
 *
 * @author Toth.Attila
 */
public abstract class Car {
    protected static int identifier = 10000;
    
    protected double ranKilemeters;
    
    protected double consumes;
    
    protected int horsePower;
    
    protected String plate;
    
    protected String color;
    
    protected int price;
    
    public Car() {
        identifier++;
    }
    
    abstract void test();

    public abstract int getIdentifier();

    public double getRanKilemeters() {
        return ranKilemeters;
    }

    public void setRanKilemeters(double ranKilemeters) {
        this.ranKilemeters = ranKilemeters;
    }

    public double getConsumes() {
        return consumes;
    }

    public void setConsumes(double consumes) {
        this.consumes = consumes;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + (int) (Double.doubleToLongBits(this.ranKilemeters) ^ (Double.doubleToLongBits(this.ranKilemeters) >>> 32));
        hash = 43 * hash + (int) (Double.doubleToLongBits(this.consumes) ^ (Double.doubleToLongBits(this.consumes) >>> 32));
        hash = 43 * hash + this.horsePower;
        hash = 43 * hash + Objects.hashCode(this.plate);
        hash = 43 * hash + Objects.hashCode(this.color);
        hash = 43 * hash + this.price;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Car other = (Car) obj;
        if (Double.doubleToLongBits(this.ranKilemeters) != Double.doubleToLongBits(other.ranKilemeters)) {
            return false;
        }
        if (Double.doubleToLongBits(this.consumes) != Double.doubleToLongBits(other.consumes)) {
            return false;
        }
        if (this.horsePower != other.horsePower) {
            return false;
        }
        if (this.price != other.price) {
            return false;
        }
        if (!Objects.equals(this.plate, other.plate)) {
            return false;
        }
        if (!Objects.equals(this.color, other.color)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Car with the following properties: " + "ranKilemeters=" + ranKilemeters + ", consumes=" + consumes + ", horsePower=" + horsePower + ", plate=" + plate + ", color=" + color + ", price=" + price;
    }
    
    
}
