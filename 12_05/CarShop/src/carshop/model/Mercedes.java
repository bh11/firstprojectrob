/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop.model;

/**
 *
 * @author Toth.Attila
 */
public class Mercedes extends Car{
    private int identifier;
    
    public Mercedes() {
        this.identifier = Car.identifier;
    }

    @Override
    void test() {
        this.consumes+=0.04;
    }

    @Override
    public int getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return "Mercedes car with " + "identifier=" + identifier + ", plate" + this.plate;
    }
    
    
    
}
