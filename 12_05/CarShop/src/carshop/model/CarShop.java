/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop.model;

import carshop.exception.CarLimitExceededException;
import carshop.exception.CarShopIsEmptyException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Toth.Attila
 */
public class CarShop {
    List<Car> cars = new ArrayList<>();
    
    private int sumPrice = 0;
    
    public void addCar(Car car) throws CarLimitExceededException {
        if(cars.size() == 5)
            throw new CarLimitExceededException("Car limit is 5!");
        cars.add(car);
        sumPrice += car.getPrice();
    }
    
    public void testCars() throws CarShopIsEmptyException {
        if(cars.isEmpty())
            throw new CarShopIsEmptyException("Car Shop is empty");
        for(Car car : cars) {
            if(car.getIdentifier() % 2 == 0) {
                car.test();
                System.out.println(car);
            }
        }
    }
    
    public int getNumberOfCars() {
        return cars.size();
    }
    
    public int getSumPriceOfCars() {
        return sumPrice;
    }
}
