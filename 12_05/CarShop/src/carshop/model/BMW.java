/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop.model;

/**
 *
 * @author Toth.Attila
 */
public class BMW extends Car{
    private int identifier;
    
    public BMW() {
        this.identifier = Car.identifier;
    }

    @Override
    void test() {
        System.out.println("BMW Test");
        this.ranKilemeters+=5000;
    }

    @Override
    public int getIdentifier() {
        return identifier;
    }
    
    @Override
    public String toString() {
        return "BMW car with " + "identifier=" + identifier + ", plate" + this.plate;
    }
    
}
