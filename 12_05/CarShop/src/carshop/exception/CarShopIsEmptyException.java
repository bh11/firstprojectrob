/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop.exception;

/**
 *
 * @author Toth.Attila
 */
public class CarShopIsEmptyException extends Exception {
    public CarShopIsEmptyException(String message) {
        super(message);
    }
}
