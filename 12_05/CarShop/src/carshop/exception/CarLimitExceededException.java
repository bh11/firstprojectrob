/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop.exception;

/**
 *
 * @author Toth.Attila
 */
public class CarLimitExceededException extends Exception{
    public CarLimitExceededException(String message) {
        super(message);
    }
    
}
