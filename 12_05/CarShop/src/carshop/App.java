/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carshop;

import carshop.exception.CarLimitExceededException;
import carshop.exception.CarShopIsEmptyException;
import carshop.model.BMW;
import carshop.model.Car;
import carshop.model.CarShop;
import carshop.model.Hammer;
import carshop.model.Mercedes;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Toth.Attila
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CarShop carShop = new CarShop();
        Car mercedes = new Mercedes();
        mercedes.setPlate("MERC-123");
        Car hammer = new Hammer();
        hammer.setPlate("HAM-123");
        Car bmw = new BMW();
        bmw.setPlate("BMW-123");
        Car hammer2 = new Hammer();
        hammer2.setPlate("HAM-124");
        Car bmw2 = new BMW();
        bmw2.setPlate("BMW-124");
        Car bmw3 = new BMW();
        bmw3.setPlate("BMW-124");
        try {
            carShop.addCar(mercedes);
            carShop.addCar(hammer);
            carShop.addCar(bmw);
            carShop.addCar(hammer2);
            carShop.addCar(bmw2);
            carShop.addCar(bmw3);
            carShop.testCars();
        } catch (CarLimitExceededException | CarShopIsEmptyException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(carShop.getNumberOfCars());
    }
    
}
