/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mostcommonnumberinarray;

/**
 *
 * @author Leo
 */
public class MostCommonNumberInArray {

    /**
     * @param args the command line arguments
     */
    
    static int[] fillEmptyArray (int[] arr) {
         for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }
         return arr;
    } 
    
    static int mostCommonElementInArray (int[] arr) {
         int[] counterArray = new int[100];
           int max = Integer.MIN_VALUE;
           
           for (int i = 0; i < arr.length; i++) {
            counterArray[arr[i]]++;
           }
           for (int i = 0; i < counterArray.length; i++) {
           if (counterArray[i] > max) {
                max = counterArray[i];
           }
        }
           return max;
    }
    public static void main(String[] args) {
        int[] array = new int[300];
 
        fillEmptyArray(array);
        System.out.println("Leggyakoribb elem: "+ mostCommonElementInArray(array));
    }

}
