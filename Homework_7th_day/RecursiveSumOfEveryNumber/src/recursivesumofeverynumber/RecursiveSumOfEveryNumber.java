/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursivesumofeverynumber;

/**
 *
 * @author Leo
 */
public class RecursiveSumOfEveryNumber {

    /**
     * @param args the command line arguments
     */
    static int sumOfEveryNumber(int num) {

        if (num == 0) {
            return 0;
        }
        return (num % 10 + sumOfEveryNumber(num / 10));
    }

    public static void main(String[] args) {
        System.out.println("321: " + sumOfEveryNumber(321));
    }

}
