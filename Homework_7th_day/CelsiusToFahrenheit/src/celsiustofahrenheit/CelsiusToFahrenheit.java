/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celsiustofahrenheit;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class CelsiusToFahrenheit {

    /**
     * @param args the command line arguments
     */
    static double celsiusToFahrenheit(int celsius) {
        double fahrenheit = (celsius * 1.8) + 32;
        return fahrenheit;
    }

    static int numberInput(String str) {
        Scanner sc = new Scanner(System.in);
        System.out.println(str);
        do {
            if (sc.hasNextInt()) {
                return sc.nextInt();
            } else {
                sc.next();
            }
        } while (true);
    }

    public static void main(String[] args) {
        int celsius = numberInput("Celsius? ");
        System.out.println(celsius + " Celsius=" + celsiusToFahrenheit(35) + " Fahrenheit");
    }

}
