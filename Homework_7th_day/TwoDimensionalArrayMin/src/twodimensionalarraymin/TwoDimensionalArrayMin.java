/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twodimensionalarraymin;

/**
 *
 * @author Leo
 */
public class TwoDimensionalArrayMin {

    /**
     * @param args the command line arguments
     */
    
    static int[][] fillArray(int n, int m) {
        int[][] arr = new int[n][m];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 100);
            }
        }
        return arr;
    }

    static int[] minInATwoDimensoinalArray(int[][] arr) {
        int[] rowColumnValue = new int[3];
        int minValue = arr[0][0];
        
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (minValue > arr[i][j]) {
                    minValue = arr[i][j];
                    
                    rowColumnValue[0]=i; //sor
                    rowColumnValue[1]=j; //oszlop
                    rowColumnValue[2]=minValue; //érték
                }
            }
        }
        return rowColumnValue;
    }

    public static void main(String[] args) {
        int[][] array;
        array = fillArray(3, 3);

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
        }
        System.out.println();
        System.out.println("sor: "+minInATwoDimensoinalArray(array)[0]);
        System.out.println("oszlop: "+minInATwoDimensoinalArray(array)[1]);
        System.out.println("érték: "+minInATwoDimensoinalArray(array)[2]);

        
    }

}
