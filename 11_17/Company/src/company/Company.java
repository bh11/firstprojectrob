/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package company;

/**
 *
 * @author Leo
 */
public class Company {

     private Employee[] employees;
    private int numberOfEmployees;
    public static double raiseIndicator = 1.1;

    
    public Company() {
        employees = new Employee[10];
    }
    
    public void addEmployee(Employee emp) {
        if(employees.length == numberOfEmployees)
            increaseMaximumNumberOfEmployees();
        employees[numberOfEmployees] = emp;
        numberOfEmployees++;
    }
    
    private void increaseMaximumNumberOfEmployees() {
        Employee[] employeesTmp = new Employee[numberOfEmployees*2];
        for(int i = 0; i < numberOfEmployees; i++) {
            employeesTmp[i] = employees[i]; 
        }
        employees = employeesTmp;
    }
    
    public void increaseSalaries() {
        for(int i = 0; i < numberOfEmployees; i++) {
            employees[i].increaseSalary(raiseIndicator);
        }
    }
    
    public void printEmployeesWithSalaries() {
        for(int i = 0; i < numberOfEmployees; i++) {
            System.out.println(employees[i].getName().getFullName() + "'s salary: " + employees[i].getSalary());
        }
    }

}
