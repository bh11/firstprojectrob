/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package company;

/**
 *
 * @author Leo
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

       Company company = new Company();
        Employee emp1 = new Employee("Bela", "Kiss", 200000);
        Employee emp2 = new Employee("Adam", "Nagy", 300000);
        Employee emp3 = new Employee("Eszter", "Móriz", 500000);
        company.addEmployee(emp1);
        company.addEmployee(emp2);
        company.addEmployee(emp3);
        
        company.printEmployeesWithSalaries();
        company.increaseSalaries();
        company.printEmployeesWithSalaries();
        
        Employee empMan=new Manager(new Name("Béla", "Menedzser"), 60000000, "Mercedes");
        Employee empProg=new Programmer(new Name("Géza", "Programozó"), 40000000);
        
        company.addEmployee(empMan);
        company.addEmployee(empProg);
        /*
        Órai végleges állapot előtti kódok:
        Company.raiseIndicator = 1.2;

        Employee employee = new Employee("Bela","Balazs","Kiss",300000.0);
        emplyoyees[0] = emplyoee;
        company.addEmployee(employee);
        System.out.println(employee.name.getFullName());
        System.out.println(employee.salary + " Ft");
        
        employee.increaseSalary();
        System.out.println(employee.salary + " Ft");
        
        Employee employee2 = new Employee("Adam","Nagy",200000.0);
        company.addEmployee(employee2);
        System.out.println(employee2.name.getFullName());
        System.out.println(employee2.salary + " Ft");
        employee2.increaseSalary();
        System.out.println(employee2.salary + " Ft");
        
        Name name = new Name("Eszter","Móricz");
        Employee employee3 = new Employee(name, 500000.0);
        System.out.println(employee3.name.getFullName());
        company.addEmployee(employee3);
        
        Employee employee4 = new Employee(new Name("Lajos","Lajvér"), 150000.0);
        System.out.println(employee4.name.getFullName());
        company.addEmployee(employee4);
        
        company.printEmployeesWithSalaries();
        company.increaseSalaries();
        company.printEmployeesWithSalaries();
        
        for(int i = 0; i < employees.length; i++) {
            employees[i].increaseSalary();
            System.out.println(employees[i].name.getFullName() + " increased salary: " + employees[i].salary + " Ft");
        }
        
        for(Employee emp : employees) {
            System.out.println(emp.name.getFullName());
        }
        
        */
    }


}
