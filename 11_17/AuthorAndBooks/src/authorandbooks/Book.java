/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the writer.
 */
package authorandbooks;

/**
 *
 * @author Leo
 */
public class Book {

    private int writeDate;
    private String title;
    private String writer;

    public Book(int editionDate, String title, String writer) {
        this.writeDate = editionDate;
        this.title = title;
        this.writer = writer;
    }

    public int getWriteDate() {
        return writeDate;
    }

    public void setWriteDate(int editionDate) {
        this.writeDate = editionDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

}
