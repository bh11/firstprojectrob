/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authorandbooks;

/**
 *
 * @author Leo
 */
public class Author {

    private String name;
    private String birthDate;
    private Book[] books = new Book[10];
    private int numberOfBooks;

    public void addBooks(Book b) {
        books[numberOfBooks] = b;
        numberOfBooks++;
    }

    public Author(String name) {
        this.name = name;
        setBirthDate("1971.01.01.");
    }

    public Author(String name, String birthDate) {
        this.name = name;
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void listBooks() {
        for (int i = 0; i < numberOfBooks; i++) {
            System.out.print(books[i].getWriteDate() + " " + books[i].getWriter() + " " + books[i].getTitle()+" \n");
        }
        System.out.println();
    }
}
