/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authorandbooks;

/**
 *
 * @author Leo
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Book book1 = new Book(19910101, "Pista könyve", "Kiss István");
        Book book2 = new Book(20081224, "Józsi könyve", "Nagy József");
        Book book3= new Book (20111024, "Pista másik könyve", "Kiss István");

        Author author1 = new Author("Kiss István");
        author1.addBooks(book1);
        author1.addBooks(book3);

        Author author2 = new Author("Nagy József", "1976. március 15.");
        author2.addBooks(book2);

        System.out.println(author1.getName());
        System.out.println(author1.getBirthDate());
        author1.listBooks();

        System.out.println(author2.getName());
        System.out.println(author2.getBirthDate());
        author2.listBooks();
    }

}
