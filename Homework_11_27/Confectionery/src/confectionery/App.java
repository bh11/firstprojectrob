/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package confectionery;

/**
 *
 * @author Leo
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AbstractSweets[] sweets = new AbstractSweets[4];

        sweets[0] = new Cake(5, "Dobos", 125);
        sweets[1] = new Cake(2, "Sacher", 547);
        sweets[2] = new BirthDayCake("Happy Birthday", 4, 2, "Orosz krém", 678);
        sweets[3] = new BirthDayCake("Isten éltessen!", 4, 1, "Fekete erdő", 593);

        for (AbstractSweets sweet : sweets) {
            System.out.println(sweet.toString());
        }
    }
}
