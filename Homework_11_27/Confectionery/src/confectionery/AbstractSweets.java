/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package confectionery;

/**
 *
 * @author Leo
 */
public abstract class AbstractSweets {

    private String nameOfCake;
    private int numberOfCalories;

    public abstract String offeredServeMode();

    public AbstractSweets(String nameOfCake, int numberOfCalories) {
        this.nameOfCake = nameOfCake;
        this.numberOfCalories = numberOfCalories;
    }

    public String getNameOfCake() {
        return nameOfCake;
    }

    public void setNameOfCake(String nameOfCake) {
        this.nameOfCake = nameOfCake;
    }

    public int getNumberOfCalories() {
        return numberOfCalories;
    }

    public void setNumberOfCalories(int numberOfCalories) {
        this.numberOfCalories = numberOfCalories;
    }

    @Override
    public String toString() {
        return "AbstractSweets{" + "nameOfCake=" + nameOfCake + ", numberOfCalories=" + numberOfCalories + '}';
    }

}
