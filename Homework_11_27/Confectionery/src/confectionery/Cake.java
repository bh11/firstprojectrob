/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package confectionery;

/**
 *
 * @author Leo
 */
public class Cake extends AbstractSweets {

    private int componentNumber;
    private final int MAX_NUMBER_OF_COMPONENTS = 5;

    public Cake(int componentNumber, String nameOfCake, int numberOfCalories) {
        super(nameOfCake, numberOfCalories);
        this.componentNumber = componentNumber;
    }

    public int getComponentNumber() {
        return componentNumber;
    }

    public void setComponentNumber(int componentNumber) {
        if (componentNumber < MAX_NUMBER_OF_COMPONENTS) {
            this.componentNumber = componentNumber;
        } else {
            System.out.println("Max. 5 components!");
        }
    }

    @Override
    public String toString() {
        return "Cake{" + "componentNumber=" + componentNumber + ", MAX_NUMBER_OF_COMPONENTS=" + MAX_NUMBER_OF_COMPONENTS + '}';
    }

    @Override
    public String offeredServeMode() {
        return "Serve this on tray";
    }

}
