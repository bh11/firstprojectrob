/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package confectionery;

/**
 *
 * @author Leo
 */
public class BirthDayCake extends Cake {

    private String BirthDayWish;
    private int componentNumberOfBirthdayCake;

    public BirthDayCake(String BirthDayWish, int componentNumberOfBirthdayCake, int componentNumber, String nameOfCake, int numberOfCalories) {
        super(componentNumber, nameOfCake, numberOfCalories);
        this.BirthDayWish = BirthDayWish;
        this.componentNumberOfBirthdayCake = componentNumberOfBirthdayCake;
    }

    public String getBirhDayWish() {
        return BirthDayWish;
    }

    public void setBirhDayWish(String BirhDayWish) {
        this.BirthDayWish = BirhDayWish;
    }

    public int getComponentNumberOfBirthdayCake() {
        return componentNumberOfBirthdayCake;
    }

    public void setComponentNumberOfBirthdayCake(int componentNumberOfBirthdayCake) {
        this.componentNumberOfBirthdayCake = componentNumberOfBirthdayCake;
    }

    @Override
    public String offeredServeMode() {
        return "Serve this sliced on plate";
    }

    @Override
    public String toString() {
        return "BirthDayCake{" + "BirthDayWish=" + BirthDayWish + ", componentNumberOfBirthdayCake=" + componentNumberOfBirthdayCake + '}';
    }

}
