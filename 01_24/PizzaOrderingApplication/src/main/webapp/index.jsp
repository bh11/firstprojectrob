<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Pizza Ordering</h1>
        <br>
        <form>
            <h2>Pizza type</h2>
            <select size="1" id="Menu" style="width:120px;color:#3342ff;">
                <option value="1">Margherita</option>
                <option value="2">Napolitano</option>
                <option value="3">Calzone</option>
                <option value="4">Stromboli</option>
                <option value="5">Deep Dish</option>
                <option value="6">Marinara</option>
            </select>
        </form>

        <h2>Pizza size</h2>
        <input type="radio" name="size" value="Ordinary"> Ordinary </input>
        <input type="radio" name="size" value="Big"> Big </input>
        <form method="post" action="SendOrderServlet"> 
            <br>
            <h2> Customer information</h2>>
            <br>
            <fieldset>
                <p>Name: <input type="text" name="name" /></p>
                <p>Address: <input type="text" name="address" /></p>   
                <input type="submit" value="Send order"/>
            </fieldset>
        </form>
    </body>
</html>
