<%-- 
    Document   : desert
    Created on : 2020.01.25., 14:20:07
    Author     : Leo
--%>

<%@page import="hu.braininghub.myfirstapi.Desert"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Deserts</title>
    </head>
    <body>
        <h1><a href="index.jsp">Home</a></h1>
        <%
            List<Desert> deserts = (List<Desert>) request.getAttribute("desertList");
            Gson gson = new Gson();
            StringBuilder sb = new StringBuilder();
            sb.append("{<br>");
            deserts.forEach(d -> {
                sb.append(gson.toJson(d)).append(",");
                sb.append("<br>");
            });
            sb.deleteCharAt(sb.length() - 5);
            sb.append("}");
            
            out.println(sb.toString());
        %>
    </body>

</html>
