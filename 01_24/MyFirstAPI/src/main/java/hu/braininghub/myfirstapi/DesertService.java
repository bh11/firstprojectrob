/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.myfirstapi;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author Leo
 */
public class DesertService {
    
    private static DesertService instance;
    private List<Desert> deserts;

    private DesertService() {
        deserts = new DesertFileReader().read();
    }

    public static synchronized DesertService getInstance() {
        if (instance == null) {
            instance = new DesertService();
        }
        return instance;
    }

    public List<Desert> getDeserts() {
        return Collections.unmodifiableList(deserts);
    }


}
