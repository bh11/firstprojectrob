/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.myfirstapi;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Leo
 */
public class GetDesertServlet extends HttpServlet {
    
 private DesertService ds = DesertService.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (ds.getDeserts() == null) {
            System.out.println("No deserts");
        }
        req.setAttribute("desertList", ds.getDeserts());
        req.getRequestDispatcher("Deserts.jsp").forward(req, resp);
    }


}
