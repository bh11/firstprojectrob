/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.myfirstapi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leo
 */
public class DesertFileReader {
    
    public static final File DATA_FILE = new File("deserts.txt");
    private final DesertParser parser;

    public DesertFileReader() {
        parser = new DesertParser();
    }

    public final List<Desert> read() {
        List<Desert> result = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(DATA_FILE))) {
            String line = br.readLine();
            System.out.println("Line: " + line);
            while (line != null) {
                try {
                    Desert desert = parser.parse(line);
                    result.add(desert);
                } catch (InvalidArgumentException ex) {
                    System.out.println("Invalid Data");
                    line = br.readLine();
                    continue;
                }
                line = br.readLine();
            }
        } catch (FileNotFoundException ex) {
            createDataFile();
            result = read();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result;
    }

    private void createDataFile() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(DATA_FILE))) {
            String str = "Kalahári|51800\n"
                    + "Nagy-homoksivatag|42000\n";
            bw.write(str);
        } catch (IOException ex) {
            System.err.println("Could not write source file!");
            ex.printStackTrace();
        }
    }


}
