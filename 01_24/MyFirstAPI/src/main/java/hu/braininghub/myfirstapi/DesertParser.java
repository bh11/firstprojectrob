/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.myfirstapi;

/**
 *
 * @author Leo
 */
public class DesertParser {
    
    private final DesertFactory factory;

    public DesertParser() {
        this.factory = new DesertFactory();
    }

    public Desert parse(String input) throws InvalidArgumentException {
        String[] inputSplit = input.split("\\|");
        if (inputSplit.length != 2) {
            System.out.println("Invalid number of elements!");
            throw new InvalidArgumentException();
        } else {
            try {
                return factory.create(inputSplit[0], Integer.parseInt(inputSplit[1]));
            } catch (NumberFormatException e) {
                throw new InvalidArgumentException();
            }

        }
    }

}
