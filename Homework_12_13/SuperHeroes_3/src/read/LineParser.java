/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import Hero.AbstractHero;
import Hero.HeroFactory;
import exceptions.InvalidNameAvengersException;
import report.Reporting;
import store.Fleet;

/**
 *
 * @author Leo
 */
public class LineParser {
    
     private static final String DELIMITER = ";";
    private static final int MIN_CHARACTERS_OF_NAME = 2;
    
    private final Fleet store = new Fleet();
    
    private final Reporting reporting = new Reporting();
    
    public void process(String line) throws InvalidNameAvengersException{
        String[] parameters = line.split(DELIMITER);
        checkNameRestriction(parameters[0]);
        AbstractHero hero = HeroFactory.create(parameters);
        store.add(hero);
    }
    
  
    public void printStore(){
        System.out.println(store.toString());
    }
     
    public void checkNameRestriction(String name) throws InvalidNameAvengersException{
        if(name.length() < MIN_CHARACTERS_OF_NAME) {
            throw new InvalidNameAvengersException("Invalid name: "+name);
        }
    }
    
    public void report(){
        
    }
    
    public void save(){
     
    }
}
