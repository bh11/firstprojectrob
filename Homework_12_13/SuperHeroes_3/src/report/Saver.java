/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import store.Fleet;

/**
 *
 * @author Leo
 */
public class Saver {

    private static final String fileName = "serFleet.ser";

    public static void serializerForFleet(Fleet f) {

        try (FileOutputStream fos = new FileOutputStream(fileName);
                ObjectOutputStream ois = new ObjectOutputStream(fos)) {
            ois.writeObject(f);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static Fleet deserializerForFleet() {
        Fleet f = null;
        try (FileInputStream fis = new FileInputStream(fileName);
                ObjectInputStream ois = new ObjectInputStream(fis)) {
            f = (Fleet) ois.readObject();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return f;
    }

}
