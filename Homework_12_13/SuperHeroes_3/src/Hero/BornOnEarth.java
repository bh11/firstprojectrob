/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hero;

import Stone.StoneType;
import abilities.Fly;

/**
 *
 * @author Leo
 */
public class BornOnEarth extends AbstractHero implements Fly {
 
    private final IdentityCard identityCard;
    
    public BornOnEarth(String name, int power, StoneType stone, IdentityCard identityCard) {
        super(name, power, stone);
        this.identityCard = identityCard;
    }

    @Override
    public void fly() {
        System.out.println("I can fly.");
    }

    public IdentityCard getIdentityCard() {
        return identityCard;
    }

    @Override
    public String toString() {
        return super.toString() + "BornOnEarth{" + "identityCard=" + identityCard + '}';
    }
}
