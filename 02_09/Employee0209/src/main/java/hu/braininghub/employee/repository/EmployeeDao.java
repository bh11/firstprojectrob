/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employee.repository;

import hu.braininghub.employee.repository.entity.Employees;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

/**
 *
 * @author Leo
 */
@Singleton
public class EmployeeDao {

    @PersistenceContext
    private EntityManager em;

    @Resource(lookup = "jdbc/lr1")
    private DataSource ds;

    @PostConstruct
    public void init() {
        System.out.println("It has been initialized");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("It has been destroyed.");
    }

    public List<Employees> getEmployees() {
        List<Employees> employees = new ArrayList<>();

        em.createQuery("SELECT e FROM Employees e", Employees.class)
                .setMaxResults(10)
                .getResultList();

        return employees;
    }

    public List<Employees> findByName(String str) {

        List<Employees> employees = new ArrayList<>();

        em.createQuery("SELECT e FROM Employees e WHERE e.firstName LIKE :pattern", Employees.class)
                .setParameter("pattern", "%" + str + "%")
                .getResultList();

        return employees;
    }
}
