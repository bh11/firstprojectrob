/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.testjunit5example.store.model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author czirjak
 */
class AirlineTest {
    
    private Airline createAirline(Airline.Airlines airline) {
        return new Airline(airline, "DUMMY");
    }
    
    @Test
    void testIsHungarianAirlineWithWizzAir() {
        //Given
        Airline underTest = createAirline(Airline.Airlines.WIZZ_AIR);
        
        //When
        boolean isHungarian = underTest.isHungarianAirline();
        
        //Then
        assertTrue(isHungarian);
    }
    
    @Test
    void testIsHungarianAirlineWithBerlinAir() {
        //Given
        Airline underTest = createAirline(Airline.Airlines.BERLIN_AIR);
        
        //When
        boolean isHungarian = underTest.isHungarianAirline();
        
        //Then
        assertFalse(isHungarian);
    }
}
