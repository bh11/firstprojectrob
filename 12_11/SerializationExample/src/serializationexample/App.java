/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializationexample;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author Leo
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Set <Employee> emp = new LinkedHashSet<>();
        Employee emp1 = new Employee("Emp1", 32, 1000000, new Department("kis u.", "IT"));
        Employee emp2 = new Employee("Emp2", 33, 764568, new Department("barack u.", "Department1"));
        Employee emp3 = new Employee("Emp3", 54, 543443, new Department("meggy u.", "Department2"));
        Employee emp4 = new Employee("Emp4", 33, 543443, new Department("szilva u.", "Department3"));
        Employee emp5 = new Employee("Emp5", 42, 378954, new Department("dinnye utca", "Department4"));
        
        emp.add(emp1);
        emp.add(emp2);
        emp.add(emp3);
        emp.add(emp4);
        emp.add(emp5);
        
        System.out.println("Employee before serialization");
        
        for (Employee employee : emp) {
            System.out.println(employee);
        }
        
        EmployeeRepository.serializeEmployee(emp);
        
        LinkedHashSet<Employee> result = EmployeeRepository.deserializeEmployee();
        System.out.println("\nEmployee after serialization");
        
        for (Employee employee : result) {
            System.out.println(employee);
        }
        System.out.println();
            
    }
    
}
