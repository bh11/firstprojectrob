/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringexcercise;

/**
 *
 * @author Leo
 */
public class StringExcercise {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String s1 = "Alma";
        s1 = s1.toLowerCase(); //toLowerCase-immutable
        System.out.println(s1);

        String s2 = " alma És nyúl\t";
        System.out.println(s2.trim().toLowerCase());

        String s3 = "Hello"; // ha van mögöttük space, akkor már egy új string keletkezik!
        String s4 = "Hello";

        if (s3.trim() == s4.trim()) {
            System.out.println("1");
        }

        StringBuilder sb = new StringBuilder("Alma");
        StringBuilder sb2 = sb.append("1");

        if (sb == sb2) {
            System.out.println("1");
        }

        String s10 = " alma körte\t";
        System.out.println("|" + s10 + "|");
        System.out.println("|" + s10.trim() + "|");
        System.out.println("|" + s10.trim().replace('a', 'X') + "|");
        System.out.println("|" + s10.trim().replace('a', 'X').indexOf('a') + "|");
        System.out.println("|" + s10.trim().replace('a', 'X').indexOf('t') + "|");
    }

}
