/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stone;

/**
 *
 * @author Leo
 */
public abstract class AbstractStone {

    private final String color;
    private final int power;

    private static final int MIN_POWER;
    private static final int MAX_POWER;

    static {
        MAX_POWER = 10;
        MIN_POWER = 2;
    }

    public AbstractStone(String color) {
        this.color = color;
        this.power = generatedPower();
    }

    public String getColor() {
        return color;
    }

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "AbstractStone{" + "color=" + color + ", power=" + power + '}';
    }

    private int generatedPower() {
        return (int) (Math.random() * (MAX_POWER - MIN_POWER) + MIN_POWER);
    }

}
