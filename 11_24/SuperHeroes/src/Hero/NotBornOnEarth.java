/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hero;

import Stone.AbstractStone;
import abilities.*;

/**
 *
 * @author Leo
 */
public class NotBornOnEarth extends AbstractHero implements Swim, Fire {

    public NotBornOnEarth(String name, int power, AbstractStone stone) {
        super(name, power, stone);
    }

    @Override
    public void swim() {
        System.out.println("I swim");
    }

    @Override
    public void fire() {
        System.out.println("I fire");
    }

}
