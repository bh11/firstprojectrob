/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Hero.AbstractHero;
import Hero.BornOnEarth;
import Hero.IdentityCard;
import Hero.NotBornOnEarth;
import Stone.Soul;
import Stone.Time;

/**
 *
 * @author Leo
 */
public class Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Soul soul = new Soul();
        Time time = new Time();

        AbstractHero[] heroes = new AbstractHero[5];

        heroes[0] = new BornOnEarth("XY", 10, soul, new IdentityCard());
        heroes[1] = new BornOnEarth("ZB", 7, time, new IdentityCard());
        heroes[2] = new NotBornOnEarth("AB", 8, time);
        heroes[3] = new NotBornOnEarth("CD", 5, soul);
        heroes[4] = new BornOnEarth("EF", 2, time, new IdentityCard());

        for (int i = 0; i < heroes.length; i++) {
            System.out.println(heroes[i]);
        }

    }

}
