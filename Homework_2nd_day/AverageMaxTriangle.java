/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package averagemaxtriangle;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class AverageMaxTriangle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Első szám: ");
        int firstNumber = sc.nextInt();
        System.out.println("Második szám: ");
        int secondNumber = sc.nextInt();
        System.out.println("Harmadik szám: ");
        int thirdNumber = sc.nextInt();

        double average = (firstNumber + secondNumber + thirdNumber) / 3.0;
        System.out.println("A három szám átlaga: " + average);

        if (firstNumber > secondNumber && firstNumber > thirdNumber) {
            System.out.println("A legnagyobb szám: " + firstNumber);
        } else if (secondNumber > firstNumber && secondNumber > thirdNumber) {
            System.out.println("A legnagyobb szám: " + secondNumber);
        } else if (thirdNumber > firstNumber && thirdNumber > secondNumber) {
            System.out.println("A legnagyobb szám: " + thirdNumber);
        } else {
            System.out.println("Legalább két szám egyenlő!");
        }

        if (firstNumber + secondNumber > thirdNumber || secondNumber + thirdNumber > firstNumber
                || firstNumber + thirdNumber > secondNumber) {
            System.out.println("Lehet háromszög!");
        } else {
            System.out.println("Nem lehet háromszög!");
        }

    }

}
