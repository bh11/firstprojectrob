/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class Calculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Első egész szám: ");
        int firstNumber = sc.nextInt();
        System.out.println("Második egész szám: ");
        int secondNumber = sc.nextInt();
        System.out.println("Harmadik egész szám: ");
        int thirdNumber = sc.nextInt();
        System.out.println("Művelet (+,-,*,/,%): ");
        String operation = sc.next();

        if (operation.equals("+")) {
            int sum = firstNumber + secondNumber + thirdNumber;
            System.out.println("Az összeg: " + sum);
        } else if (operation.equals("-")) {
            int difference = firstNumber - secondNumber - thirdNumber;
            System.out.println("A különbség: " + difference);
        } else if (operation.equals("*")) {
            int product = firstNumber * secondNumber * thirdNumber;
            System.out.println("A szorzat: " + product);
        } else if (operation.equals("/") && (firstNumber == 0 || secondNumber == 0
                || thirdNumber == 0)) {
            System.out.println("0-val nem osztunk!");
        } else if (operation.equals("/")) {
            double ratio = 1.0 * firstNumber / secondNumber / thirdNumber;
            System.out.println("A hányados: " + ratio);
        } else if (operation.equals("%") && (firstNumber == 0 || secondNumber == 0
                || thirdNumber == 0)) {
            System.out.println("0-val nem képezünk maradékot!");
        } else if (operation.equals("%")){
            int remainder = firstNumber % secondNumber % thirdNumber;
            System.out.println("A maradék: " + remainder);
        } else {
            System.out.println("Nem értelmezhető művelet!");
        }
    }

}
