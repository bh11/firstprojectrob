/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dowhile;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class DoWhile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        do {
            System.out.println("Legalább egyszer lefut");

        } while (false);

        Scanner sc = new Scanner(System.in);
        int stopNumber = 0;
        int number;
        do {
            number = sc.nextInt();
            if (number != stopNumber) {
                System.out.println(number);
            }

        } while (number != stopNumber);
    }

}
