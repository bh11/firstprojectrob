/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triangle;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class Triangle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("A oldal:");
        int a = sc.nextInt();
        System.out.println("B oldal:");
        int b = sc.nextInt();
        System.out.println("C oldal:");
        int c = sc.nextInt();

        if (a + b > c || a + c > b || b + c > a) {
            if (a == b || a == c || b == c) {
                System.out.println("Egyenlőszárú háromszög");
            } else {
                System.out.println("Nem egyenlőszárú háromszög");

            }
        } else {
            System.out.println("Nem háromszög");
        }
    }

}
