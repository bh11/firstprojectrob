/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passedornot;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class PassedOrNot {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        if (sc.hasNextInt()) {
            int number = sc.nextInt();

            switch (number) {
                case 5:
                    System.out.println("Jeles");
                    break;
                case 4:
                    System.out.println("Jó");
                    break;
                case 3:
                    System.out.println("Közepes");
                    break;
                case 2:
                    System.out.println("Elégséges");
                    break;
                default:
                    System.out.println("Elégtelen");
            }

            if (number == 5) {
                System.out.println("Jeles");
            } else if (number == 4) {
                System.out.println("Jó");
            } else if (number == 3) {
                System.out.println("Közepes");
            } else if (number == 2) {
                System.out.println("Elégséges");
            } else {
                System.out.println("Elégtelen");
            }

            boolean hasNotPassed = number == 1 || number == 2 || number == 3;
            if (hasNotPassed) {
                System.out.println("Nem felelt meg");
            } else {
                System.out.println("Megfelelt");
            }
            switch (number) {
                case 5:
                case 4:
                    System.out.println("Megfelelt");
                    break;
                case 3:
                case 2:
                case 1:
                    System.out.println("Nem felelt meg");
            }
        }
    }

}
