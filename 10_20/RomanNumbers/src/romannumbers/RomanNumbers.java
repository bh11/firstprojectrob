/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package romannumbers;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class RomanNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean asking = true;
        int number = -1;

        do {
            System.out.println("Írj be egy számot 1-5-ig!");
            if (sc.hasNextInt()) {
                number = sc.nextInt();

                if (number >= 1 && number <= 5) {
                    asking = false;
                    
                    switch (number) {
                        case 1: System.out.println(number+ ". szám római megfelelője: I.");
                            break;
                        case 2: System.out.println(number+ ". szám római megfelelője: II.");
                            break;
                        case 3: System.out.println(number+ ". szám római megfelelője: III.");
                            break;
                        case 4: System.out.println(number+ ". szám római megfelelője: IV.");
                            break;
                        case 5: System.out.println(number+ ". szám római megfelelője: V.");
                    }
                }
            } else {
                sc.next();
            }

        } while (asking);
        System.out.println(number);
    }

}
