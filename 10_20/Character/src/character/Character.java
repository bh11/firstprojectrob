/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package character;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class Character {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc=new Scanner (System.in);
        System.out.println("Írj be egy karaktert!");
        char c = sc.next().charAt(0);
        int ascii = (int) c;
        
        if (ascii>=48 && ascii<=57) {
            System.out.println("A karakter szám");
        } else if (ascii>=65 && ascii<=90) {
            System.out.println("A karakter nagybetű");
        } else if (ascii>=97 && ascii<=122) {
            System.out.println("A karakter kisbetű");
        } else {
            System.out.println("A karakter se nem kisbetű, se nem nagybetű, se nem szám");
        }
        
    }
    
}
