/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mountainorplain;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class MountainOrPlain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc=new Scanner (System.in);
        System.out.println("Írj be egy számot!");
        
        int number = sc.nextInt();
        
        int helpForSwitch=0;
        
        if (number>=0 && number <=200) {
            System.out.println("Alföld");
        } else if (number>=201 && number <=500) {
            helpForSwitch=1;
            System.out.println("Dombság");
        } else if (number>=501 && number <=1500) {
            helpForSwitch=2;
            System.out.println("Középhegység");
        } else {
            helpForSwitch=3;
            System.out.println("Hegység");
        }
        
        switch (helpForSwitch) {
            case 0: System.out.println("Alföld");
                    break;
            case 1: System.out.println("Dombság");
                    break;
            case 2: System.out.println("Középhegység");
                    break;
            case 3: System.out.println("Hegység");
                    break;
        }
    }
    
}
