/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgswitch;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class Switch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner sc=new Scanner (System.in);
        
        if (sc.hasNextInt()) {
        int number=sc.nextInt();
        
        switch (number) {
            case 5: System.out.println("Jeles");
                break;
            case 4: System.out.println("Jó");
                break;
            case 3: System.out.println("Közepes");
                break;
            case 2: System.out.println("Elégséges");
                break;
            default: System.out.println("Elégtelen");
        }
        
        if (number==5) {
            System.out.println("Jeles");
        } else if (number==4) {
            System.out.println("Jó");
        } else if (number==3) {
            System.out.println("Közepes");
        } else if (number==2) {
            System.out.println("Elégséges");
        } else {
            System.out.println("Elégtelen");
        }
    }
    }
}

