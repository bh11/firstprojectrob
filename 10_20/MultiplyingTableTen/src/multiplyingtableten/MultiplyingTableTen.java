/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiplyingtableten;

/**
 *
 * @author Leo
 */
public class MultiplyingTableTen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int i = 10;
        int j = 1;

        while (i <= 100) {

            System.out.println(j + " x 10 = " + i);
            j++;
            i += 10;
        }
    }
}
