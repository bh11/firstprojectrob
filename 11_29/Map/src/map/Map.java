/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package map;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leo
 */
public class Map {

    /**
     * @param args the command line arguments
     */
    public static boolean isPrime(int n) {
        if (n == 1) {
            return false;
        }
        for (int j = 1; j <= Math.sqrt(n); j++) {
            if (n % j == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        
        List<Integer> primeNumbers = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            if (isPrime(i)) {
                primeNumbers.add(i);
            }
        }
        System.out.println(primeNumbers);
    }

}
