/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bubblesort;

/**
 *
 * @author Leo
 */
public class BubbleSort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int[] testArray = {20, 9, 3, 16, -5, 66};

        for (int i = testArray.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (testArray[j] > testArray[j + 1]) {
                    int tmp = testArray[j];
                    testArray[j] = testArray[j + 1];
                    testArray[j + 1] = tmp;
                }
            }
        }
        for (int i = 0; i < testArray.length; i++) {
            System.out.print(testArray[i] + " ");
        }
        System.out.println();
    }
}
