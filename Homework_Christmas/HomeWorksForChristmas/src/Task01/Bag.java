/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task01;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leo
 */
public class Bag<T> {
    private List<T> l= new ArrayList<T>();

    public List<T> getList() {
        return l;
    }

    public void setList(List<T> list) {
        this.l = list;
    }
    
    public boolean addAnElements(T el){
        return l.add(el);
    }
    
    public boolean removeAnElement(T el){
        return l.remove(el);
    }

    @Override
    public String toString() {
        return " " + l;
    }
}
