/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task01;

/**
 *
 * @author Leo
 */
public class App {
    
    public static void main(String[] args) {
        Bag <String> bag = new Bag();
        
        bag.addAnElements("Alma");
        bag.addAnElements("Barack");
        bag.addAnElements("Dinnye");
        
        System.out.println(bag.toString());
        
        bag.removeAnElement("Barack");
        System.out.println(bag.toString());
        
    }
}
