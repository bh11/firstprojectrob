/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task18.producer;

import java.io.Serializable;

/**
 *
 * @author Leo
 */
public class Producer implements Serializable{
    
    private String name;
    private String address;

    public Producer(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "producer{" + "name=" + name + ", address=" + address + '}';
    }
}
