/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task18.reader;

import Task18.alcohol.AbstractAlcohol;
import Task18.alcohol.AlcoholFactory;
import Task18.report.Report;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leo
 */
public class LineParser {
     private static final String DELIMITER = " ";
    
    private final List <AbstractAlcohol> store = new ArrayList<>();
    private Report report = new Report();
    
    public void process(String line){
        String[] parameters = line.split(DELIMITER);
        AbstractAlcohol alcohol = AlcoholFactory.create(parameters);
        store.add(alcohol);
    }
    
    public void report(){
        report.report(store);
    }
    
    public List getWineListToSave(){
        return report.wineListToSave(store);
    }
    
}
