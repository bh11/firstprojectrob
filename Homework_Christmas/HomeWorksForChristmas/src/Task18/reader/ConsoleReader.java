/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task18.reader;

import Task18.saver.SaveWine;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Leo
 */
public class ConsoleReader {

    private int count = 0;
    private static final String STOP = "SAVE";
    private LineParser pars = new LineParser();
    
    public void read() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            while (count <= 100 ) {                
                String line = br.readLine();
                if (STOP.equalsIgnoreCase(line) || line == null) {
                    pars.report();
                    SaveWine.serializationWine(pars.getWineListToSave());
                    break;
                }
                pars.process(line);
                count++;
            }  
        } catch (IOException ex) {  
            System.out.println(ex);  
        }
    }
}
