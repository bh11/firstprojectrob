/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task18.alcohol;


import Task18.numbergenerator.NumberGenerator;
import Task18.producer.Producer;
import java.time.LocalDateTime;

/**
 *
 * @author Leo
 */
public class GermanAlcohol extends AbstractAlcohol{
    
    private String label;
    private String createLabelDate;
    
    NumberGenerator randomLabel = new NumberGenerator();

    public GermanAlcohol(AlcoholType type, Producer manufacturer, int price, int alcoholPercent, String countryType) {
        super(type, manufacturer, price, alcoholPercent, countryType);
        this.label = randomLabel.labelGenerator();
        this.createLabelDate = LocalDateTime.now().toString();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCreateLabelDate() {
        return createLabelDate;
    }

    @Override
    public String toString() {
        return super.toString() + "GermanAlcohol{" + "label=" + label + ", createLabelDate=" + createLabelDate + '}';
    }
}

