/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task18.alcohol;

import Task18.producer.Producer;

/**
 *
 * @author Leo
 */
public class HungarianAlcohol extends AbstractAlcohol {

    public HungarianAlcohol(AlcoholType type, Producer manufacturer, int price, int alcoholPercent, String countryType) {
        super(type, manufacturer, price, alcoholPercent, countryType);
    }

    public void extenuate() {
        setAlcoholContent((int) (getAlcoholContent() / 2));
    }

    public void drinkAlcohol() {
        System.out.println("Drink");
    }

    @Override
    public String toString() {
        return super.toString() + "HungarianAlcohol{" + '}';
    }
}
