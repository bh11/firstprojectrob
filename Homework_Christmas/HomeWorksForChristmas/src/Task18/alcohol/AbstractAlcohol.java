/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task18.alcohol;

import Task18.numbergenerator.NumberGenerator;
import Task18.producer.Producer;
import java.io.Serializable;

/**
 *
 * @author Leo
 */
public class AbstractAlcohol implements Comparable<AbstractAlcohol>, Serializable {

    private int id;
    private AlcoholType type;
    private Producer producer;
    private String country;
    private int alcoholContent;
    private int price;

    NumberGenerator generator = new NumberGenerator();

    public AbstractAlcohol() {
    }

    public AbstractAlcohol(AlcoholType type, Producer prod, int price, int alcoholContent, String country) {
        this.id = generator.generatorForId();
        this.type = type;
        this.producer = prod;
        this.price = price;
        this.alcoholContent = alcoholContent;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AlcoholType getType() {
        return type;
    }

    public void setType(AlcoholType type) {
        this.type = type;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public int getAlcoholContent() {
        return alcoholContent;
    }

    public void setAlcoholContent(int alcoholContent) {
        this.alcoholContent = alcoholContent;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "AbstractAlcohol{" + "id=" + id + ", type=" + type + ", producer=" + producer + ", country=" + country + ", alcoholContent=" + alcoholContent + ", price=" + price + '}';
    }

    @Override
    public int compareTo(AbstractAlcohol id) {
        return this.id - id.getId();
    }
}
