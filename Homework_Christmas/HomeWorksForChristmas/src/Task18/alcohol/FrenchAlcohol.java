/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task18.alcohol;

import Task18.exception.FreezeException;
import Task18.producer.Producer;

/**
 *
 * @author Leo
 */
public class FrenchAlcohol extends AbstractAlcohol{
    
    private boolean frozen;

    public FrenchAlcohol(AlcoholType type, Producer producer, int price, int alcoholContent, String country) {
        super(type, producer, price, alcoholContent, country);
    }
    
    public boolean freeze(){
        return this.frozen = true;
    }

    public boolean isFrozen() {
        return frozen;
    }

    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }
    
    public void transport() throws FreezeException{
        if (freeze()) {
            throw new FreezeException();
        }
        System.out.println("Frozen, transport is impossible!");
    }

    @Override
    public String toString() {
        return super.toString() + "FrenchAlcohol{" + "frozen=" + frozen + '}';
    }
}
