/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task18.alcohol;

import Task18.producer.Producer;

/**
 *
 * @author Leo
 */
public class AlcoholFactory {
    public static AbstractAlcohol create(String [] param){
          
        AlcoholType type = AlcoholType.valueOf(param[0].toUpperCase());
        Producer producer = new Producer(param[1], "address");
        int price = Integer.parseInt(param[2]);
        int alcoholContent = Integer.parseInt(param[3]);
        String country = param[4];
        
        if ("hungarian".equals(country.toLowerCase())) {
            return new HungarianAlcohol(type, producer, price, alcoholContent, country);
        }
        
        if ("german".equals(country.toLowerCase())) {
            return new GermanAlcohol(type, producer, price, alcoholContent, country);
        }
        
        if ("french".equals(country.toLowerCase())) {
            return new FrenchAlcohol(type, producer, price, alcoholContent, country);
        }  
        return null;
    }
}
