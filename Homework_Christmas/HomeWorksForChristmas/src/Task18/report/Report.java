/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task18.report;

import Task18.alcohol.AbstractAlcohol;
import Task18.alcohol.AlcoholType;
import Task18.alcohol.FrenchAlcohol;
import Task18.alcohol.GermanAlcohol;
import Task18.alcohol.HungarianAlcohol;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Leo
 */
public class Report {

    public void report(List<AbstractAlcohol> store) {

        System.out.println("Number of saved wine: ");
        System.out.println(numberOfSavedWine(store));
        System.out.println("Product number by group: ");
        System.out.println("Hungarian products is: " + groupProducts(store)[0]);
        System.out.println("German products is: " + groupProducts(store)[1]);
        System.out.println("French products is: " + groupProducts(store)[2]);
        System.out.println("Wine number in store: " + numberOfWine(store));
        System.out.println("Producer by the store: " + producerByStore(store));

    }

    public long numberOfSavedWine(List<AbstractAlcohol> store) {
        long wineNumber
                = store.stream()
                .filter(p -> AlcoholType.WINE.equals(p.getType()))
                .count();

        return wineNumber;
    }

    public long[] groupProducts(List<AbstractAlcohol> store) {
        long[] groupProduct = new long[3];

        groupProduct[0]
                = store.stream()
                .filter(p -> p instanceof HungarianAlcohol)
                .count();
        groupProduct[1]
                = store.stream()
                .filter(p -> p instanceof GermanAlcohol)
                .count();
        groupProduct[2]
                = store.stream()
                .filter(p -> p instanceof FrenchAlcohol)
                .count();

        return groupProduct;
    }

    public long numberOfWine(List<AbstractAlcohol> store) {
        long wineNumber
                = store.stream()
                .filter(p -> AlcoholType.WINE.equals(p.getType()))
                .count();
        return wineNumber;
    }

    public List producerByStore(List<AbstractAlcohol> store) {
        List producerByStore
                = store.stream()
                .map(p -> p.getProducer().getName())
                .distinct()
                .collect(Collectors.toList());
        return producerByStore;
    }

    public List wineListToSave(List<AbstractAlcohol> store) {
        List wineList
                = store.stream()
                .filter(p -> AlcoholType.WINE.equals(p.getType()))
                .collect(Collectors.toList());
        return wineList;
    }
    
}
