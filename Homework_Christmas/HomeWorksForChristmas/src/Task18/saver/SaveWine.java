/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task18.saver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 *
 * @author Leo
 */
public class SaveWine {
    
    private static final String WINEFILE = "wine.ser";
    
    
    public static void serializationWine(List wine){
        try (FileOutputStream fs = new FileOutputStream(WINEFILE) ){
             ObjectOutputStream ou = new ObjectOutputStream(fs);
             ou.writeObject(wine);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public static List deserializationWine(){
        List wine = null;
        try (FileInputStream fs = new FileInputStream(WINEFILE)){
            ObjectInputStream oi = new ObjectInputStream(fs);
            wine = (List) oi.readObject();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return wine;
    }
}

