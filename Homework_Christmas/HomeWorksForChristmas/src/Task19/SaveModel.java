/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task19;

import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 *
 * @author Leo
 */
public class SaveModel {

    private static final String FILE_NAME = "out.txt";

    public void writeToFile(Integer numA, Integer numB, Operator op) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILE_NAME))) {

            if (numB > 0 && op.getCharacter() != null) {
                bw.write(numA.toString() + op.getCharacter() + numB.toString());
            } else if (numA > 0) {
                bw.write(numA.toString());
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
