/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task19;

import java.util.Objects;

/**
 *
 * @author Leo
 */
public class Model {

    private Controller controller;

    private String expression = new String();

    private int number1;
    private int number2;

    private Operator operator;

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;

        controller.notifyView();
    }

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.controller);
        hash = 41 * hash + Objects.hashCode(this.expression);
        hash = 41 * hash + this.number1;
        hash = 41 * hash + this.number2;
        hash = 41 * hash + Objects.hashCode(this.operator);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Model other = (Model) obj;
        if (this.number1 != other.number1) {
            return false;
        }
        if (this.number2 != other.number2) {
            return false;
        }
        if (!Objects.equals(this.expression, other.expression)) {
            return false;
        }
        if (!Objects.equals(this.controller, other.controller)) {
            return false;
        }
        if (this.operator != other.operator) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model{" + "controller=" + controller + ", expression=" + expression + ", number1=" + number1 + ", number2=" + number2 + ", operator=" + operator + '}';
    }

}
