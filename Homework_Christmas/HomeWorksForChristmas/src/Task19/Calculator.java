/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task19;

/**
 *
 * @author Leo
 */
public class Calculator {

    public static void main(String[] args) {

        Model m = new Model();
        View v = new View();
        Controller c = new Controller(v, m);

        v.showWindow();
    }
}
