/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task19;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Leo
 */
public class View extends JFrame {

    private Controller cont;

    private JTextField jtf = new JTextField(30);

    public void setController(Controller controller) {
        this.cont = controller;
    }

    public void showWindow() {
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void setText(String expr) {
        jtf.setText(expr);
    }

    private JPanel buildNorth() {
        jtf.setEditable(false);
        JPanel jp = new JPanel();
        jp.add(jtf);

        return jp;
    }

    private JPanel buildCenter() {
        JPanel jp = new JPanel();
        jp.setLayout(new GridLayout(4, 3));

        int value = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                JButton jb = new JButton(String.valueOf(value++));
                jp.add(jb);

                jb.addActionListener(l -> {
                    JButton button = (JButton) l.getSource();
                    int nr = Integer.parseInt(button.getText());

                    cont.setNumber(nr);
                });
            }
        }

        addOps(jp);

        return jp;
    }

    private void addOps(JPanel jp) {
        JButton add = new JButton("+");
        JButton subst = new JButton("-");
        JButton multip = new JButton("*");
        JButton div = new JButton("/");
        JButton eq = new JButton("=");
        JButton mem = new JButton("M+");

        add.addActionListener(l -> cont.handleOp(Operator.ADD));
        subst.addActionListener(l -> cont.handleOp(Operator.SUBTRACTION));
        multip.addActionListener(l -> cont.handleOp(Operator.MULTIPLY));
        div.addActionListener(l -> cont.handleOp(Operator.DIVISON));
        eq.addActionListener(l -> cont.handleOp(Operator.EQ));
        mem.addActionListener(l -> cont.saveModel());

        jp.add(add);
        jp.add(subst);
        jp.add(multip);
        jp.add(div);
        jp.add(eq);
        jp.add(mem);
    }
}
