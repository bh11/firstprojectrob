/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task12;

/**
 *
 * @author Leo
 */
public class Car {
    
    private String plate;
    private int id;
    private String model;
    private String color;
    private boolean abs;
    
    

    public static class CarBuilder {

        private String plate;
        private int id;
        private String model;
        private String color;
        private boolean abs;
        
        

        public CarBuilder(String model, String color) {
            this.model = model;
            this.color = color;

        }

        public void setPlate(String plate) {
            this.plate = plate;
        }

        public void setId(int id) {
            this.id = id;
        }

        public CarBuilder setAbs(boolean abs) {
            this.abs = abs;
            return this;
        }

        public Car build() {
            return new Car(this);
        }
    }

    public Car(CarBuilder cb) {
        this.plate=cb.plate;
        this.id=cb.id;
        this.model = cb.model;
        this.color = cb.color;
        this.abs = cb.abs;
        
    }


}
