/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import collector.FileCollector;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.FilterGenerator;
import filter.FilterAggregator;
import java.io.File;
import java.util.List;
import reader.ConsoleReader;

/**
 *
 * @author Leo
 */
public class App {
    
    private FileCollector fileCollector=new FileCollector();
    private ConsoleReader consoleReader = new ConsoleReader();
    private FilterAggregator filterAggregator=new FilterAggregator();
            
    public void process() {
        String path=consoleReader.read();
        List<File> files=fileCollector.collect(new File(path));
        List<File> filteredFiles=filterAggregator.filter(files);
        
        printResult(filteredFiles);
    }
    
    private void printResult (List<File> files) {
        files.forEach(System.out::println);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        App app= new App();
        app.process();
    }
    
}
