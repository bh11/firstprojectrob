/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import filter.handlers.Filter;
import filter.handlers.SizeLimited2MbFiter;
import filter.handlers.SmallLetterContainedFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Leo
 */
public class FilterAggregator {

    private final List<Filter> filters = new ArrayList<>();

    public FilterAggregator() {
        filters.add(new SizeLimited2MbFiter());
        filters.add(new SmallLetterContainedFilter());
    }

    public List<File> filter(List<File> files) {
        return files.stream()
                .filter(this::allFilterMAtched)
                .collect(Collectors.toList());
    }

    private boolean allFilterMAtched(File f) {
        for (Filter filter : filters) {
            if (!filter.filter(f)) {
                return false;
            }
        }
        return true;
    }

    private List<Filter> collectRelevantFilters(File file) {
        return filters.stream()
                .filter(f -> f.test(file))
                .collect(Collectors.toList());
    }
}
