/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handlers;

import java.io.File;

/**
 *
 * @author Leo
 */
public class SizeLimited2MbFiter implements Filter {

    @Override
    public boolean filter(File file) {
        return convertToMegaByteFromByte(file.length()) < 2D;
    }
    
    private double convertToMegaByteFromByte(long value) {
        return value/1024D/1024D;
    }

    @Override
    public boolean test(File file) {
        return true;
    }

}
