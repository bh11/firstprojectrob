/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handlers;

import java.io.File;


/**
 *
 * @author Leo
 */
public interface Filter {
    boolean filter(File file);
    boolean test(File file);
    
}
