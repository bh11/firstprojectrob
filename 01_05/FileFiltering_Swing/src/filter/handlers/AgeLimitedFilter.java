/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handlers;

import java.io.File;

/**
 *
 * @author Leo
 */
public class AgeLimitedFilter implements Filter {

    private static final String USER_HOME = System.getProperty("user.home");

    @Override
    public boolean filter(File file) {
        return convertMillisecToDay(file.lastModified()) < 2D; 
    }
    
    private double convertMillisecToDay (long value) {
        return value / 1000D / 3600D / 24D;
    }

    @Override
    public boolean test(File file) {
        return file.getPath().contains(USER_HOME);
    }

}
