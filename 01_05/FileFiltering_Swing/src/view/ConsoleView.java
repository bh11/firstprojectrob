/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ViewController;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Leo
 */
public class ConsoleView implements View{
    
    private ViewController controller;
    private static final String STOP = "exit";
    private static String filePath = null;

    @Override
    public void setController(ViewController controller) {
        this.controller = controller;
    }

    @Override
    public void update(List<File> files) {
        System.out.println(
                "Filter result:\n"
                + files.stream()
                .map(File::getPath)
                .collect(Collectors.joining("\n"))
        );
    }

    @Override
    public void enableView() {
        while (!STOP.equalsIgnoreCase(filePath)) {
            controller.handleGoButton(readingFromConsole());
            enableView();
        }
    }

    private String readingFromConsole() {
        System.out.println("Directory for filtered list: ");
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                filePath = br.readLine();
                if (filePath == null || STOP.equalsIgnoreCase(filePath)) {
                    br.close();
                    return null;
                   
                }
                return filePath;
            }

        } catch (IOException ex) {
            System.out.println("Invalid directory");
            ex.printStackTrace();
            return null;
        }

    }


}
