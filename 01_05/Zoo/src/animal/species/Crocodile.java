/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal.species;

import animal.AbstractReptile;
import animal.feature.Aquatic;
import animal.feature.Terrestrial;

/**
 *
 * @author Leo
 */
public class Crocodile extends AbstractReptile implements Terrestrial,Aquatic{

   
    public Crocodile(String name, int requiredPlace) {
        super(name, requiredPlace);
    }
    
}
