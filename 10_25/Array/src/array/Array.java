/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package array;

/**
 *
 * @author Leo
 */
public class Array {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] numbers = {3, 6, 8};
        int[] numbers2 = new int[10];

        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
        System.out.println(numbers[2]);

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        double[] doubleArray = {1.4, 5.6, 9.8};
        
        float[] floatArray = new float[4];

    }

}
