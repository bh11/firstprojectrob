/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primenumbers;

import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class PrimeNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean isPrime=true;
       Scanner sc =new Scanner (System.in);
        System.out.println("Szám: ");
       int number=sc.nextInt();
       
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number%i==0) {
                isPrime=false;
            }
        }
        if (!isPrime || number==1) {
            System.out.println("Nem prím");
        } else {
            System.out.println("Prím");
        }
    }
    
}
