/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numberofzeros;

/**
 *
 * @author Leo
 */
public class NumberOfZeros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int fives=0;
        int num=5;
        int intervalEnd=100;
        
        for (int i = 1; i < intervalEnd; i++) {
            int number =i;
            while(number%5==0) {
                fives++;
                number/=5;
            }
        }
        System.out.println("Ötös: " + fives);
    }
    
}
