/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anotherarray;

/**
 *
 * @author Leo
 */
public class AnotherArray {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int n = 10;
        int k = n;
        k++;

        System.out.println("n: " + n);

        int[] numbers = {2, 3, 4};
        int[] karray = numbers;
        karray[1]++;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(karray[i] + "-" + numbers[i]);
        }

        karray = new int[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            karray[i] = numbers[i];
        }

        karray[1]++;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(karray[i] + "-" + numbers[i]);
        }

        //10 elemű tömb, 1-100 rnd, írjuk ki a tömb mximumát és indexét
        int[] tenArray = new int[10];

        for (int i = 0; i < tenArray.length; i++) {
            tenArray[i] = (int) (Math.random() * 100) + 1;
        }
        for (int i = 0; i < tenArray.length; i++) {
            System.out.print(tenArray[i] + ",");
        }

        int max = Integer.MIN_VALUE;
        int index = 0;
        for (int i = 0; i < tenArray.length; i++) {
            if (tenArray[i] > max) {
                max = tenArray[i];
                index = i;
            }
        }
        System.out.println();
        System.out.println("Maximum: " + max);
        System.out.println("Index: " + index);

        int[][] multiArray = new int[10][10];
        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                multiArray[i][j] = i * j;
            }
        }

        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                System.out.print(" " + multiArray[i][j]);
            }
            System.out.println("");
        }
        int[][] ma = {{2, 3}, {0, 1}};
    }
}
