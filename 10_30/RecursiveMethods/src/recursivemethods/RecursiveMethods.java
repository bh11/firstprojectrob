/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursivemethods;

/**
 *
 * @author Leo
 */
public class RecursiveMethods {
    
    static void m(int a) {
        
        if(a==0) {
            return;
        }
        System.out.println(a);
        m(--a);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        m(10);
    }
    
}
