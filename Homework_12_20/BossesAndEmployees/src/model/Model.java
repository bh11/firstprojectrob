/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import people.EmployeeStore;


/**
 *
 * @author Leo
 */
public class Model {
    private Controller cont;
    private EmployeeStore emp;

    public void setController(Controller cont) {
        this.cont = cont;
    }

 
    public void setBossName(String bossName) {
        emp.createBoss(bossName);
    }
    
    public void setEmployeeName (String employeeName) {
        emp.createEmployee(employeeName);
    }
    
    public void serialize(){
        emp.serializeEmployeeList();
    }
    
    public String getEmployeeList(){
        return emp.listEmployees();
    }
}
