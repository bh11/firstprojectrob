/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.util.Scanner;

/**
 *
 * @author Leo
 */
public class ConsoleView implements View {
    
    private final String STOP="exit";
    private Controller cont;
    
     private void startConsoleReader() {
        try (Scanner scanner = new Scanner(System.in)) {
            String typeOfEmployee;

            do {
                System.out.print("Type of Emloyees: \nB: Boss \nE: Employee \nL: List \nS: save \nexit: exit program \n");
                typeOfEmployee = scanner.nextLine();

                if (STOP.equals(typeOfEmployee)) {
                    break;
                }

                if ("B".equals(typeOfEmployee)) {
                    System.out.println("Boss name: ");
                    String bossName;
                    bossName = scanner.nextLine();
                    cont.setBossName(bossName);
                }
                
                if ("E".equals(typeOfEmployee)) {
                    System.out.println("Employee name: ");
                    String employeeName;
                    employeeName = scanner.nextLine();
                    cont.setEmployeeName(employeeName);
                }
                
                if ("L".equals(typeOfEmployee)) {
                    cont.getEmp();
                }
                
                if ("S".equals(typeOfEmployee)) {
                    cont.notifyForSerializeRequest();
                }

            } while (true);
        }
    }

    @Override
    public void start() {
         startConsoleReader();
    }

    @Override
    public void setController(Controller c) {
        cont=c;
    }

    @Override
    public void setText(String s) {
        
    }
    
}
