/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Leo
 */
public class SwingView extends JFrame implements View{
    
    private Controller cont;
    
    private JTextField jtf=new JTextField("Name: ", 4);
    private JTextField nameTextField=new JTextField(25);
    private JTextField showEmployees=new JTextField(40);
    private final JButton bossButton = new JButton("Boss");
    private final JButton  employeeButton= new JButton("Employee");
    private final JButton serializeButton = new JButton("Serialize");
    private final JButton listButton = new JButton("List");
    
    
    public void buildWindow() {
        add(buildNorth(),BorderLayout.NORTH);
        add(buildCenter(),BorderLayout.CENTER);
        add(buildSouth(),BorderLayout.SOUTH); 
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    private JPanel buildNorth() {
        JPanel jp=new JPanel();
        jp.add(jtf);
        jp.add(nameTextField);
        jtf.setEditable(false);
        nameTextField.setEditable(true);
   
        return jp;
    }
    
    private JPanel buildCenter(){
        JPanel jp=new JPanel();
        jp.add(listButton);
        jp.add(bossButton);
        jp.add(employeeButton);
        jp.add(serializeButton);
        
        listButton.addActionListener(l-> {
            showEmployees.setText (cont.getEmp());
                });
        bossButton.addActionListener(l-> cont.setBossName(nameTextField.getText()));
        employeeButton.addActionListener(l->cont.setEmployeeName(nameTextField.getText()));
        serializeButton.addActionListener(l->cont.notifyForSerializeRequest());
        
        return jp;
    }
    
    private JPanel buildSouth(){
        JPanel jp=new JPanel();
        jp.add(showEmployees);
        showEmployees.setEditable(false);

        return jp;
    }

    @Override
    public void start() {
       buildWindow();
    }

    @Override
    public void setController(Controller c) {
       cont=c;
    }

    @Override
    public void setText(String s) {
       showEmployees.setText(s);
    }
    
}
