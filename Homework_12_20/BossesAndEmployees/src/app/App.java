/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import controller.Controller;
import model.Model;
import view.ConsoleView;
import view.SwingView;
import view.View;

/**
 *
 * @author Leo
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Model m=new Model();
        View v=new SwingView();
       // View v=new ConsoleView();
        Controller c=new Controller(v, m);
        
        v.setController(c);
        m.setController(c);
        
        v.start();
    }
    
}
