/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Model;
import view.View;

/**
 *
 * @author Leo
 */
public class Controller {
    private View v;
    private Model m;

    public Controller(View v, Model m) {
        this.v = v;
        this.m = m;
    }
    
    public void setBossName (String bossName) {
             m.setBossName(bossName);
    }
    
    public void setEmployeeName (String employeeName) {
            m.setEmployeeName(employeeName);
    }
    
    public void notifyForSerializeRequest() {
        m.serialize();
    }

    public void notifyingView (){
        v.setText(m.getEmployeeList());
    }
    
    public String getEmp() {
        return m.getEmployeeList();
    }
}
