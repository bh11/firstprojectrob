/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeefactory;

import people.AbstractEmployee;
import people.Bosses;
import people.Employees;

/**
 *
 * @author Leo
 */
public class EmployeeFactory {
    
     public AbstractEmployee createEmployee(String type){
        if ("E".equals(type)) {
            return new Employees();
        }
        if ("B".equals(type)) {
            return new Bosses();
        }
        return null;
    }
}
