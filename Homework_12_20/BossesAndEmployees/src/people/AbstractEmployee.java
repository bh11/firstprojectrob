/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Leo
 */
public abstract class AbstractEmployee implements Serializable {
    
    private String name;

    public AbstractEmployee() {
    }

    public AbstractEmployee(String Name) {
        this.name = Name;
    }

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractEmployee other = (AbstractEmployee) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AbstractEmployee{" + "Name=" + name + '}';
    }
    
    
}
