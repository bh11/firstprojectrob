/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people;

import employeefactory.EmployeeFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import saver.Saver;

/**
 *
 * @author Leo
 */
public class EmployeeStore implements Serializable{
    private List<AbstractEmployee> employees = new ArrayList<>();
    private EmployeeFactory emp;
    
    Saver save = new Saver();
    
    public List deserializeEmployeeList(){
        return save.deserializerForEmployees();
    }

    public List<AbstractEmployee> getEmployees() {
        return employees;
    }
    
    public void createBoss(String bossName){
        employees = deserializeEmployeeList();
        AbstractEmployee boss = emp.createEmployee("B");
        employees.add(boss);
        boss.setName(bossName);
        serializeEmployeeList();
    }
    
    public void createEmployee(String employeeName){
        employees = deserializeEmployeeList();
        AbstractEmployee employee = emp.createEmployee("E");
        employees.add(employee);
        employee.setName(employeeName);
        serializeEmployeeList();
    }
    
    public void serializeEmployeeList(){
        save.serializerForEmployees(employees);
    }
    
    public String listEmployees(){
        return employees.toString();
    }
}
