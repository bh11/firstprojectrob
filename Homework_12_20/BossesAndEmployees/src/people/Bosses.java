/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leo
 */
public class Bosses extends AbstractEmployee implements Serializable {
    private String name;
    private List<Employees> employees=new ArrayList<>();
    private String sub;

    public Bosses(String Name) {
        super(Name);
    }

    public Bosses() {
        
    }
    public String getName (){
        return name;
    }
    
    public void stName (String name) {
        this.name=name;
    }

    public List<Employees> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employees> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
         employees.stream()
               .forEach(p -> {
                   sub += p.getName() + " ,";
               });
        return "Bosses{" + "name=" + name + ", employees=" + sub + '}';
    }
}
