/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people;

import java.io.Serializable;

/**
 *
 * @author Leo
 */
public class Employees extends AbstractEmployee implements Serializable{
    private Bosses boss;

    public Employees(String Name) {
        super(Name);
    }
    public Employees() {
        
    }

    public Bosses getBoss() {
        return boss;
    }

    public void setBoss(Bosses boss) {
        this.boss = boss;
    }

    @Override
    public String toString() {
        return "Employees{" + "boss=" + boss + '}';
    }
    
}
