/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Leo
 */
public class NameWriter extends Thread {

    private final String name;
    private final String neptun;

    public NameWriter(String name, String neptun) {
        this.name = name;
        this.neptun = neptun;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
                System.out.println(this.name + " " + this.neptun);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
                Date date = new Date(System.currentTimeMillis());
                System.out.println(sdf.format(date));

            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

}
