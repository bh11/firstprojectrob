/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

/**
 *
 * @author Leo
 */
public class Threads {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException{
       
        NameWriter t1 = new NameWriter("t1 Leopold Róbert", "LEOROB");
        NameWriter t2 = new NameWriter("t2 Leopold Róbert", "LEOROB");
        NameWriter t3 = new NameWriter("t3 Leopold Róbert", "LEOROB");
        NameWriter t4 = new NameWriter("t4 Leopold Róbert", "LEOROB");
        NameWriter t5 = new NameWriter("t5 Leopold Róbert", "LEOROB");
        
        t1.start();
        t1.join();
        t2.start();
        t2.join(1000);
        t3.start();
        t3.join();
        t4.start();
        t5.start();

    }
    
}
