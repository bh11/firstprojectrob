/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import Hero.AbstractHero;
import Hero.BornOnEarth;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Leo
 */
public class Reporting {

   
    public long getCountOfBornOnEarth(List<AbstractHero> heroes) {
        return heroes.stream()
                .filter(h -> h instanceof BornOnEarth)
                .count();
    }
    
   
    public AbstractHero strongestSuperhero(List<AbstractHero> heroes){
        return heroes.stream()
                .sorted((a, b) -> b.getPower() - a.getPower())
                .findFirst().get();
    }
    
   
    public List<Integer> identityCardNumbers(List<AbstractHero> heroes){
        return heroes.stream()
                .filter(h -> h instanceof BornOnEarth)
                .map(i -> ((BornOnEarth)i).getIdentityCard().getNumber()).collect(Collectors.toList());
    }


}
