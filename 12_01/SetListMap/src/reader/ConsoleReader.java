/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import com.sun.xml.internal.ws.streaming.XMLStreamReaderUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Logger;
import store.StoreAdd;
import store.StringStore;

/**
 *
 * @author Leo
 */
public class ConsoleReader {

    private static final String STOP = "exit";

    public void readScanner(StoreAdd store) {
        try (Scanner sc = new Scanner(System.in)) {
            String word = null;
            do {
                word = sc.next();
                store.add(word);

            } while (!STOP.equals(word));
        }
    }

    public void readByInputStreamReader(StoreAdd store) {

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String line = br.readLine();
                if (line == null || STOP.equals(line)) {
                    break;
                }
                store.add(line);
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
}
