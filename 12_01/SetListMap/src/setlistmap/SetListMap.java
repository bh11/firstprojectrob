/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setlistmap;

import reader.ConsoleReader;
import report.Reporting;
import store.StringStore;

/**
 *
 * @author Leo
 */
public class SetListMap {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StringStore store = new StringStore();
        ConsoleReader reader = new ConsoleReader();
        Reporting report = new Reporting(store);
        reader.readByInputStreamReader(store);
        report.generateRiports();
    }

}
