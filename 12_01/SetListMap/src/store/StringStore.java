/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leo
 */
public class StringStore implements Store, GetList {

    private final List<String> strings = new ArrayList<>();

    @Override
    public void add(String s) {
        strings.add(s);
    }

    @Override
    public void remove(String s) {
        strings.remove(s);
    }

    @Override
    public List<String> getData() {
        return strings;
    }
}
