/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import store.GetList;

/**
 *
 * @author Leo
 */
public class Reporting {
    
    private final List<String> data;
    
    public Reporting(GetList store) {
        this.data = store.getData();
    }
    
    public void generateRiports() {
        reportUniqueWords();
        reportReversedItems();
        reportGroupedItems();
    }
    
    private void reportUniqueWords() {
        System.out.println("********************************");
        Set<String> words = new HashSet<>(data);
        System.out.println("Egyedi szavak száma: " + words.size());
        System.out.println("Egyedi szavak: ");
        
        for (String s : words) { //foreach eseténa kollekció módosítása nem lehetséges (csak bejárható)!
            System.out.println(s);
        }
        
    }
    
    private void reportReversedItems() {
        System.out.println("********************************");
        System.out.println("Szavak fordított sorrendben: ");
        
        for (int i = data.size() - 1; i >= 0; i--) //fordítva adja vissza a lista elemeit
        {
            System.out.println(data.get(i));
        }
    }
    
    private void buildGroupedItems(Map<Integer, List<String>> map) {
        for (String i : data) {
            int key = i.length();
            
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<>());
            }
            map.get(key).add(i);
        }
    }
    
    private void reportGroupedItems() {
        System.out.println("********************************");
        Map<Integer, List<String>> wordsByLength = new HashMap<>();
        buildGroupedItems(wordsByLength);
        
        for (int i : wordsByLength.keySet()) {
            System.out.println(i + " szavak: " + wordsByLength.get(i));
        }
    }
}
