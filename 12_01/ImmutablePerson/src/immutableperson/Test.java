/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package immutableperson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Leo
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Child> children = new ArrayList<>();
        children.add(new Child(10, "Jancsi"));
        children.add(new Child(9, "Juliska"));

        ImmutableParent parent = new ImmutableParent("Anna", 30, children);
        ImmutableParent parent2 = new ImmutableParent("Botond", 30, children);
        ImmutableParent parent3 = new ImmutableParent("Anna", 40, children);

        //parent.getChildren().add(new Child(4, "Béla"));
        Set<ImmutableParent> parents = new TreeSet<>();
        parents.add(parent);
        parents.add(parent2);
        parents.add(parent3);

        for (ImmutableParent p : parents) {
            System.out.println(p);
        }

        Child c1 = new Child(10, "Jancsi");
        Child c2 = new Child(10, "Juliska");
        Child c3 = new Child(5, "Pista");

        Set<Child> childrenTreeSet = new TreeSet<>(new ChildAgeComparator());

        childrenTreeSet.add(c1);
        childrenTreeSet.add(c2);
        childrenTreeSet.add(c3);

        for (Child child : childrenTreeSet) {
            System.out.println(child);
        }
        System.out.println("*************************************************");
        Set<Child> childrenTreeSetByName = new TreeSet<>(new ChildNameComparator());

        childrenTreeSetByName.add(c1);
        childrenTreeSetByName.add(c2);
        childrenTreeSetByName.add(c3);

        for (Child child : childrenTreeSetByName) {
            System.out.println(child);
        }
//legidősebb gyerek
        Set<Child> kids = new HashSet<>();
        kids.add(c1);
        kids.add(c2);
        kids.add(c3);

        Child oldest = Collections.max(kids, new ChildAgeComparator());
        System.out.println("**************************************");
        System.out.println(oldest);
//töröljük ki a legidősebb gyereket
        
        Iterator<Child> ic=kids.iterator();
        while (ic.hasNext()) {
            Child child=ic.next();
            if (child.equals(oldest)) {
                ic.remove();
            }
        }
        System.out.println("******************************");
        System.out.println("The oldest has been removed: ");
        for (Child kid : kids) {
            System.out.println(kid);
        }
        
        
    }

}
