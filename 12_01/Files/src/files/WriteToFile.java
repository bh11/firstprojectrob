/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package files;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Leo
 */
public class WriteToFile {

    public void write(String line) {
        try (
                FileWriter fw = new FileWriter("fw.txt");
                PrintWriter pw = new PrintWriter(fw)
                
             ) {
            
            pw.println(line);

        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
}
